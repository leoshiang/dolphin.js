﻿QUnit.module('Dolphin.DateTime.DateRange.clear');

QUnit.test('清除之後起訖日期為 null', function(assert) {
  var startDate = new Date('2018/1/1');
  var endDate = new Date('2018/1/3');
  var dateRange = new Dolphin.DateTime.DateRange(startDate, endDate);
  dateRange.clear();
  assert.equal(dateRange.startDate, null);
  assert.equal(dateRange.endDate, null);
});
