﻿QUnit.module('Dolphin.DateTime.DateRange.intersects');

QUnit.test('開始日期重疊', function(assert) {
  var r1 = new Dolphin.DateTime.DateRange(new Date('2018/1/1'), new Date('2018/1/31'));

  var r2 = new Dolphin.DateTime.DateRange(new Date('2018/1/20'), new Date('2018/2/31'));

  assert.ok(r1.intersects(r2));
});

QUnit.test('結束日期重疊', function(assert) {
  var r1 = new Dolphin.DateTime.DateRange(new Date('2018/1/1'), new Date('2018/1/31'));

  var r2 = new Dolphin.DateTime.DateRange(new Date('2018/1/20'), new Date('2017/2/31'));

  assert.ok(r1.intersects(r2));
});

QUnit.test('包含', function(assert) {
  var r1 = new Dolphin.DateTime.DateRange(new Date('2018/1/1'), new Date('2018/1/31'));

  var r2 = new Dolphin.DateTime.DateRange(new Date('2018/1/2'), new Date('2017/1/3'));

  assert.ok(r1.intersects(r2));
});

QUnit.test('相同', function(assert) {
  var r1 = new Dolphin.DateTime.DateRange(new Date('2018/1/1'), new Date('2018/1/31'));

  var r2 = new Dolphin.DateTime.DateRange(new Date('2018/1/1'), new Date('2017/1/31'));

  assert.ok(r1.intersects(r2));
});
