﻿QUnit.module('Dolphin.DateTime.DateRange.contains');

QUnit.test('日期在區間內', function(assert) {
  var startDate = new Date('2018/1/1');
  var endDate = new Date('2018/1/31');
  var dateRange = new Dolphin.DateTime.DateRange(startDate, endDate);
  assert.equal(dateRange.contains(new Date('2018/1/2')), true);
});

QUnit.test('日期在區間之前', function(assert) {
  var startDate = new Date('2018/1/1');
  var endDate = new Date('2018/1/31');
  var dateRange = new Dolphin.DateTime.DateRange(startDate, endDate);
  assert.equal(dateRange.contains(new Date('2017/1/2')), false);
});

QUnit.test('日期在區間之後', function(assert) {
  var startDate = new Date('2018/1/1');
  var endDate = new Date('2018/1/31');
  var dateRange = new Dolphin.DateTime.DateRange(startDate, endDate);
  assert.equal(dateRange.contains(new Date('2019/1/2')), false);
});

QUnit.test('日期在邊界', function(assert) {
  var startDate = new Date('2018/1/1');
  var endDate = new Date('2018/1/31');
  var dateRange = new Dolphin.DateTime.DateRange(startDate, endDate);
  assert.equal(dateRange.contains(new Date('2018/1/1')), true);
  assert.equal(dateRange.contains(new Date('2018/1/31')), true);
});
