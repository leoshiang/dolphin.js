﻿QUnit.module('Dolphin.DateTime.DateRange.setRange');

QUnit.test('設定起訖日', function(assert) {
  var range = new Dolphin.DateTime.DateRange();
  var d1 = new Date('2018/1/20');
  var d2 = new Date('2018/2/31');
  range.setRange(d1, d2);
  assert.equal(range.startDate.getTime(), d1.getTime());
  assert.equal(range.endDate.getTime(), d2.getTime());
});
