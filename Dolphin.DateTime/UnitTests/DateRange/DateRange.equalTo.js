﻿QUnit.module('Dolphin.DateTime.DateRange.equalTo');

QUnit.test('複製', function(assert) {
  var startDate = new Date('2018/1/1');
  var endDate = new Date('2018/1/3');
  var dateRange = new Dolphin.DateTime.DateRange(startDate, endDate);
  var clone = dateRange.clone();
  assert.ok(dateRange.equalTo(clone));
});
