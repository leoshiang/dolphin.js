﻿QUnit.module('Dolphin.DateTime.DateRange');

QUnit.test('確認成員有被定義', function(assert) {
  assert.equal(typeof Dolphin.DateTime.DateRange, 'function');
  assert.equal(typeof Dolphin.DateTime.DateRange.prototype.clear, 'function');
  assert.equal(typeof Dolphin.DateTime.DateRange.prototype.clone, 'function');
  assert.equal(typeof Dolphin.DateTime.DateRange.prototype.contains, 'function');
  assert.equal(typeof Dolphin.DateTime.DateRange.prototype.equalTo, 'function');
  assert.equal(typeof Dolphin.DateTime.DateRange.prototype.getDuration, 'function');
  assert.equal(typeof Dolphin.DateTime.DateRange.prototype.intersects, 'function');
  assert.equal(typeof Dolphin.DateTime.DateRange.prototype.setRange, 'function');
  assert.equal(typeof Dolphin.DateTime.DateRange.prototype.union, 'function');
});

QUnit.test('Constructor 無參數', function(assert) {
  var dateRange = new Dolphin.DateTime.DateRange();
  assert.equal(dateRange.startDate, null);
  assert.equal(dateRange.endDate, null);
});

QUnit.test('Constructor 起迄日', function(assert) {
  var startDate = new Date('2018/1/1');
  var endDate = new Date('2018/1/3');
  var dateRange = new Dolphin.DateTime.DateRange(startDate, endDate);
  assert.equal(dateRange.startDate, startDate);
  assert.equal(dateRange.endDate, endDate);
});
