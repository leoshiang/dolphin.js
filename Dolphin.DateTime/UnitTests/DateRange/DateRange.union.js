﻿QUnit.module('Dolphin.DateTime.DateRange.union');

QUnit.test('未重疊的日期聯集', function(assert) {
  var r1 = new Dolphin.DateTime.DateRange(new Date('2018/1/1'), new Date('2018/1/31'));

  var r2 = new Dolphin.DateTime.DateRange(new Date('2018/1/20'), new Date('2018/2/31'));

  var union = r1.union(r2);

  assert.equal(union.startDate.getTime(), r1.startDate.getTime());
  assert.equal(union.endDate.getTime(), r2.endDate.getTime());
});
