﻿QUnit.module('Dolphin.DateTime.DateRange.getDuration');

QUnit.test('起訖日同一天', function(assert) {
  var dateRange = new Dolphin.DateTime.DateRange(new Date('2018/1/1'), new Date('2018/1/1'));
  assert.equal(dateRange.getDuration(), 1);
});

QUnit.test('起訖日不同天', function(assert) {
  var dateRange = new Dolphin.DateTime.DateRange(new Date('2018/1/1'), new Date('2018/1/3'));
  assert.equal(dateRange.getDuration(), 3);
});
