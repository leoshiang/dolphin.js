﻿QUnit.module('Dolphin.DateTime');

QUnit.test('確認成員有被定義', function(assert) {
  assert.equal(typeof Dolphin.DateTime, 'object');
  assert.equal(typeof Dolphin.DateTime.daysToSec, 'function');
  assert.equal(typeof Dolphin.DateTime.daysToMSec, 'function');
});
