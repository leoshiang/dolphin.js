﻿QUnit.module('Dolphin.DateTime.daysToSec');

QUnit.test('0天應回傳0', function(assert) {
  assert.equal(Dolphin.DateTime.daysToSec(0), 0);
});

QUnit.test('1天應回傳 60 * 60 * 24', function(assert) {
  assert.equal(Dolphin.DateTime.daysToSec(1), 60 * 60 * 24);
});
