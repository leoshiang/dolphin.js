﻿QUnit.module('Dolphin.DateTime.daysToMSec');

QUnit.test('0天應回傳0', function(assert) {
  assert.equal(Dolphin.DateTime.daysToMSec(0), 0);
});

QUnit.test('1天應回傳 60 * 60 * 24 * 1000', function(assert) {
  assert.equal(Dolphin.DateTime.daysToMSec(1), 60 * 60 * 24 * 1000);
});
