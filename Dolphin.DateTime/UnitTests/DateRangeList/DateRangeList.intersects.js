﻿QUnit.module('Dolphin.DateTime.DateRangeList.intersects');

QUnit.test('四個日期應該有3個交集', function(assert) {
  var list = new Dolphin.DateTime.DateRangeList();
  list.add([
    new Dolphin.DateTime.DateRange(new Date('2018/1/1'), new Date('2018/1/2')),
    new Dolphin.DateTime.DateRange(new Date('2018/1/3'), new Date('2018/1/4')),
    new Dolphin.DateTime.DateRange(new Date('2018/1/5'), new Date('2018/1/6')),
    new Dolphin.DateTime.DateRange(new Date('2018/1/7'), new Date('2018/1/7'))
  ]);
  var range = new Dolphin.DateTime.DateRange(new Date('2018/1/2'), new Date('2018/1/6'));
  var result = list.intersection(range);
  assert.equal(result.count(), 3);
});
