﻿QUnit.module('Dolphin.DateTime.DateRangeList');

QUnit.test('確認成員有被定義', function(assert) {
  assert.equal(typeof Dolphin.DateTime.DateRangeList, 'function');
  assert.equal(typeof Dolphin.DateTime.DateRangeList.prototype.add, 'function');
});

QUnit.test('Constructor 無參數', function(assert) {
  var list = new Dolphin.DateTime.DateRangeList();
  assert.ok(list.items !== undefined);
});
