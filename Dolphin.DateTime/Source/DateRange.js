Dolphin.namespace('DateTime').DateRange = function() {
  this.startDate = null;
  this.endDate = null;
  if (arguments.length === 2) {
    this.setRange(arguments[0], arguments[1]);
  }
};

Dolphin.DateTime.DateRange.prototype = {
  clear: function() {
    this.startDate = null;
    this.endDate = null;
  },

  clone: function() {
    return new Dolphin.DateTime.DateRange(this.startDate, this.endDate);
  },

  contains: function(date) {
    return date >= this.startDate && date <= this.endDate;
  },

  equalTo: function(range) {
    return this.startDate === range.startDate && this.endDate === range.endDate;
  },

  getDuration: function() {
    var oneDay = Dolphin.DateTime.daysToMSec(1);
    var endDay = Math.floor(this.endDate.getTime() / oneDay);
    var startDay = Math.floor(this.startDate.getTime() / oneDay);
    return endDay - startDay + 1;
  },

  intersects: function(range) {
    var containsStartDate = this.contains(range.startDate) && !this.contains(range.endDate);
    var containsEndDate = !this.contains(range.startDate) && this.contains(range.endDate);
    var inside = this.contains(range.startDate) && this.contains(range.endDate);
    var contains = range.startDate <= this.startDate && range.endDate >= this.endDate;
    return containsStartDate || containsEndDate || inside || contains;
  },

  union: function(range) {
    var startDate = new Date(Math.min(this.startDate.getTime(), range.startDate.getTime()));
    var endDate = new Date(Math.max(this.endDate.getTime(), range.endDate.getTime()));
    return new Dolphin.DateTime.DateRange(startDate, endDate);
  },

  setRange: function(startDate, endDate) {
    this.startDate = startDate;
    this.endDate = endDate;
  }
};
