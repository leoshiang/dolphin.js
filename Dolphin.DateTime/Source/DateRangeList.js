Dolphin.namespace('DateTime').DateRangeList = function() {
  this.items = new Dolphin.Collections.List();
  this.bounds = new Dolphin.DateTime.DateRange();
};

Dolphin.DateTime.DateRangeList.prototype = {
  addArray: function(array) {
    Dolphin.Assert(array, Array);
    this.beginUpdate();
    try {
      array.forEach(
        function(item) {
          this.addRange(item);
        }.bind(this)
      );
    } catch (err) {
      console.log(err);
    } finally {
      this.endUpdate();
    }
    return this;
  },

  addList: function(list) {
    Dolphin.Assert(list, Dolphin.Collections.List);
    this.beginUpdate();
    try {
      list.forEach(
        function(item) {
          this.addRange(item);
        }.bind(this)
      );
    } catch (err) {
      console.log(err);
    } finally {
      this.endUpdate();
    }
    return this;
  },

  addRange: function(range) {
    Dolphin.Assert(range, Dolphin.DateTime.DateRange);
    this.items.add(range);
    this.onUpdated();
    return this;
  },

  intersection: function(range) {
    Dolphin.Assert(range, Dolphin.DateTime.DateRange);
    var result = new Dolphin.Collections.List();
    this.items.forEach(function(item) {
      if (item.intersects(range)) {
        result.add(item);
      }
    });
    return result;
  },

  sort: function() {
    this.items.sort(function(a, b) {
      if (a.startDate < b.startDate) {
        return -1;
      } else if (a.startDate > b.startDate) {
        return 1;
      } else {
        return 0;
      }
    });
    return this;
  },

  onUpdated: function() {
    if (this.updating) {
      return;
    }

    if (this.items.isEmpty()) {
      this.bounds.clear();
    } else {
      this.bounds = this.items.get(0);
      this.items.forEach(
        function(item) {
          this.bounds = this.bounds.union(item);
        }.bind(this)
      );
    }
  }
};

Dolphin.Collections.ListMixin.call(Dolphin.DateTime.DateRangeList.prototype);
Dolphin.Collections.LockingMixin.call(Dolphin.DateTime.DateRangeList.prototype);
