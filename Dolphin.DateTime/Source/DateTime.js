Dolphin.namespace('').DateTime = (function() {
  function daysToSec(days) {
    return 60 * 60 * 24 * days;
  }

  function daysToMSec(days) {
    return 1000 * daysToSec(days);
  }

  return {
    daysToSec: daysToSec,
    daysToMSec: daysToMSec
  };
})();
