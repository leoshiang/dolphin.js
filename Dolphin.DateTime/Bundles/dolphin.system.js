function Namespace() {}

Namespace.prototype = {
  traverse: function(namespace, callback) {
    if (namespace.trim() === '') {
      return this;
    }

    var parts = namespace.split('.');
    for (var i = 0; i < parts.length; i++) {
      var part = parts[i].trim();
      if (part === '') {
        throw new Dolphin.Exceptions.InvalidNamespace();
      }
      parts[i] = part;
    }

    var node = this;
    parts.forEach(function(name) {
      node = callback(node, name);
    });

    return node;
  },

  namespace: function(namespace) {
    return this.traverse(namespace, function(node, item) {
      node[item] = node[item] || {};
      return node[item];
    });
  },

  reflection: function(namespace) {
    return this.traverse(namespace, function(node, item) {
      return node[item];
    });
  }
};

var Dolphin = new Namespace();
Dolphin.namespace('Exceptions').AssertionError = function(message) {
  this.message = message || 'Assert exception.';
  this.name = 'AssertionError';
  this.stack = new Error().stack;
};
Dolphin.Exceptions.AssertionError.prototype = Object.create(Error.prototype);
Dolphin.Exceptions.AssertionError.prototype.constructor = Dolphin.Exceptions.AssertionError;
Dolphin.namespace('Exceptions').IndexOutOfRange = function(message) {
  this.message = message || '索引值超出範圍';
  this.name = 'IndexOutOfRange';
  this.stack = new Error().stack;
};
Dolphin.Exceptions.IndexOutOfRange.prototype = Object.create(Error.prototype);
Dolphin.Exceptions.IndexOutOfRange.prototype.constructor = Dolphin.Exceptions.IndexOutOfRange;
Dolphin.namespace('Exceptions').InvalidArgument = function(message) {
  this.message = message || '無效的參數';
  this.name = 'InvalidArgument';
  this.stack = new Error().stack;
};
Dolphin.Exceptions.InvalidArgument.prototype = Object.create(Error.prototype);
Dolphin.Exceptions.InvalidArgument.prototype.constructor = Dolphin.Exceptions.InvalidArgument;
Dolphin.namespace('Exceptions').InvalidNamespace = function(message) {
  this.message = message || '無效的命名空間';
  this.name = 'InvalidNamespace';
  this.stack = new Error().stack;
};
Dolphin.Exceptions.InvalidNamespace.prototype = Object.create(Error.prototype);
Dolphin.Exceptions.InvalidNamespace.prototype.constructor = Dolphin.Exceptions.InvalidNamespace;
Dolphin.namespace('Exceptions').NonExistentElement = function(message) {
  this.message = message || '項目不存在';
  this.name = 'NonExistentElement';
  this.stack = new Error().stack;
};
Dolphin.Exceptions.NonExistentElement.prototype = Object.create(Error.prototype);
Dolphin.Exceptions.NonExistentElement.prototype.constructor = Dolphin.Exceptions.NonExistentElement;
Dolphin.namespace('Exceptions').NullException = function(message) {
  this.message = message || '空值錯誤';
  this.name = 'NullException';
  this.stack = new Error().stack;
};
Dolphin.Exceptions.NullException.prototype = Object.create(Error.prototype);
Dolphin.Exceptions.NullException.prototype.constructor = Dolphin.Exceptions.NullException;
Dolphin.namespace('').Types = {
  Boolean: 'boolean',
  Number: 'number',
  String: 'string',
  Object: 'object',
  Null: 'null',
  Undefined: 'undefined',
  Function: 'function',
  Array: 'array'
};

Dolphin.namespace('').getType = function(object) {
  var result;
  if (object === null) {
    result = Dolphin.Types.Null;
  } else if (object instanceof Array) {
    result = Dolphin.Types.Array;
  } else {
    result = typeof object;
  }
  return result;
};
Dolphin.namespace('').asString = function(object) {
  var dolphinTypes = Dolphin.Types;
  switch (Dolphin.getType(object)) {
    case dolphinTypes.Array:
      return 'array[' + object.length + ']';

    case dolphinTypes.Null:
      return 'null';

    case dolphinTypes.Undefined:
      return 'undefined';

    case dolphinTypes.Boolean:
      return 'boolean:' + object;

    case dolphinTypes.String:
      return "string:'" + object + "'";

    case dolphinTypes.Object:
      return 'object:' + JSON.stringify(object);

    case dolphinTypes.Number:
      return 'number:' + object;

    default:
      return 'Unknown';
  }
};

Dolphin.namespace('').deepEqual = function(object1, object2) {
  if (typeof object1 !== typeof object2) {
    return false;
  }

  if (typeof object1 === 'function') {
    return object1.toString() === object2.toString();
  }

  if (object1 instanceof Object && object2 instanceof Object) {
    if (Dolphin.propertyCount(object1) !== Dolphin.propertyCount(object2)) {
      return false;
    }
    var r = true;
    for (k in object1) {
      r = Dolphin.deepEqual(object1[k], object2[k]);
      if (!r) {
        return false;
      }
    }
    return true;
  } else {
    return object1 === object2;
  }
};

Dolphin.namespace('').getArgumentNames = function(func) {
  var ARROW = true;
  var FUNC_ARGS = ARROW
    ? /^(function)?\s*[^\(]*\(\s*([^\)]*)\)/m
    : /^(function)\s*[^\(]*\(\s*([^\)]*)\)/m;
  var FUNC_ARG_SPLIT = /,/;
  var FUNC_ARG = /^\s*(_?)(.+?)\1\s*$/;
  var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/gm;

  return ((func || '')
    .toString()
    .replace(STRIP_COMMENTS, '')
    .match(FUNC_ARGS) || ['', '', ''])[2]
    .split(FUNC_ARG_SPLIT)
    .map(function(arg) {
      return arg.replace(FUNC_ARG, function(all, underscore, name) {
        return name.split('=')[0].trim();
      });
    })
    .filter(String);
};

Dolphin.namespace('').getObjectSize = function(object) {
  if (Dolphin.isEmpty(object)) {
    return 0;
  }
  return JSON.stringify(object).length;
};

Dolphin.namespace('').isEmpty = function(object) {
  var dolphinTypes = Dolphin.Types;
  switch (Dolphin.getType(object)) {
    case dolphinTypes.Array:
      return object.length < 1;

    case dolphinTypes.Null:
      return true;

    case dolphinTypes.Undefined:
      return true;

    case dolphinTypes.Boolean:
      return !object;

    case dolphinTypes.String:
      return object.length < 1;

    case dolphinTypes.Object:
      return Object.keys(object).length < 1;

    case dolphinTypes.Number:
      return object === 0;

    default:
      return false;
  }
};

Dolphin.namespace('').isNull = function(object) {
  return object === null || object === undefined;
};

Dolphin.namespace('').propertyCount = function(object) {
  var count = 0;
  for (var key in object) {
    if (object.hasOwnProperty(key)) {
      count++;
    }
  }
  return count;
};
