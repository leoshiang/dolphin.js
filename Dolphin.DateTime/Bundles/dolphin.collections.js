Dolphin.namespace('Collections').CollectionMixin = function() {
  this.clear = function() {
    this.items = [];
    return this;
  };

  this.count = function() {
    return this.items.length;
  };

  this.isEmpty = function() {
    return this.items.length === 0;
  };

  return this;
};
Dolphin.namespace('Collections').ListMixin = function() {
  this.add = function(item) {
    this.items.push(item);
    return this;
  };

  this.append = function(list) {
    this.ensureList(list);
    this.items = this.items.concat(list.items);
    return this;
  };

  this.assign = function(list) {
    this.ensureList(list);
    this.items = list.items.slice();
    return this;
  };

  this.clone = function() {
    return new Dolphin.Collections.List(this);
  };

  this.delete = function(index) {
    this.ensureIndex(index);
    this.items.splice(index, 1);
    return this;
  };

  this.difference = function(list) {
    this.ensureList(list);
    var result = new Dolphin.Collections.List();
    this.forEach(function(item) {
      if (!list.has(item)) {
        result.add(item);
      }
    });
    return result;
  };

  this.distinct = function() {
    var result = new Dolphin.Collections.List();
    this.items.forEach(function(item) {
      if (!result.has(item)) {
        result.add(item);
      }
    });
    return result;
  };

  this.ensureFunction = function(func) {
    Dolphin.Assert.instanceOf(func, Function, TypeError);
  };

  this.ensureIndex = function(index) {
    Dolphin.Assert.inRange(index, 0, this.items.length - 1, Dolphin.Exceptions.IndexOutOfRange);
  };

  this.ensureList = function(list) {
    Dolphin.Assert.isNotNull(list, Dolphin.Exceptions.NullException);
    Dolphin.Assert.instanceOf(list, Dolphin.Collections.List, TypeError);
  };

  this.equal = function(source) {
    if (source instanceof Array) {
      return (
        source.length === this.items.length &&
        this.every(function(item, index) {
          return Dolphin.deepEqual(source[index], item);
        })
      );
    }

    this.ensureList(source);
    if (source.count() !== this.count()) {
      return false;
    }

    var result = true;
    for (var i = 0; i < this.items.length; i++) {
      if (!Dolphin.deepEqual(this.items[i], source.items[i])) {
        return false;
      }
    }

    return result;
  };

  this.every = function(callback) {
    this.ensureFunction(callback);
    return this.items.every(callback);
  };

  this.exchange = function(index1, index2) {
    this.ensureIndex(index1);
    this.ensureIndex(index2);
    var temp = this.items[index1];
    this.items[index1] = this.items[index2];
    this.items[index2] = temp;
    return this;
  };

  this.find = function(callback) {
    return this.items.find(callback);
  };

  this.findIndex = function(callback) {
    return this.items.findIndex(callback);
  };

  this.filter = function(callback) {
    this.ensureFunction(callback);
    return new Dolphin.Collections.List(this.items.filter(callback));
  };

  this.forEach = function(callback) {
    this.ensureFunction(callback);
    this.items.forEach(callback);
    return this;
  };

  this.get = function(index) {
    this.ensureIndex(index);
    return this.items[index];
  };

  this.has = function(item) {
    return this.indexOfItem(item) !== -1;
  };

  this.indexOf = function(item) {
    return this.items.indexOf(item);
  };

  this.indexOfItem = function(target) {
    for (var i = 0; i < this.items.length; i++) {
      if (Dolphin.deepEqual(this.items[i], target)) {
        return i;
      }
    }
    return -1;
  };

  this.insert = function(index, item) {
    this.ensureIndex(index);
    this.items.splice(index, 0, item);
    return this;
  };

  this.intersection = function(otherList) {
    this.ensureList(otherList);
    var result = new Dolphin.Collections.List();
    for (var i = 0; i < this.items.length; i++) {
      var item = this.items[i];
      if (otherList.has(item)) {
        result.items.push(item);
      }
    }
    return result;
  };

  this.map = function(callback) {
    this.items.map(callback);
  };

  this.move = function(currentIndex, newIndex) {
    this.ensureIndex(currentIndex);
    this.ensureIndex(newIndex);
    var item = this.items[currentIndex];
    this.insert(newIndex, item);
    if (newIndex < currentIndex) {
      this.delete(currentIndex + 1);
    } else {
      this.delete(currentIndex);
    }
    return this;
  };

  this.put = function(index, item) {
    this.ensureIndex(index);
    this.items[index] = item;
    return this;
  };

  this.remove = function(item) {
    var index = this.indexOf(item);
    if (index >= 0) {
      this.items.splice(index, 1);
      return true;
    }
    return false;
  };

  this.reverse = function() {
    var result = [];
    for (var i = this.items.length - 1; i >= 0; i--) {
      result.push(this.items[i]);
    }
    this.items = result.slice();
    return this;
  };

  this.sort = function(comparer) {
    this.ensureFunction(comparer);
    this.items.sort(comparer);
    return this;
  };

  this.subset = function(list) {
    this.ensureList(list);
    for (var i = 0; i < this.items.length; i++) {
      if (!list.has(this.items[i])) {
        return false;
      }
    }
    return true;
  };

  this.union = function(list) {
    this.ensureList(list);
    var result = this.distinct();
    for (var i = 0; i < list.items.length; i++) {
      var otherItem = list.items[i];
      if (!result.has(otherItem)) {
        result.items.push(otherItem);
      }
    }
    return result;
  };
};
Dolphin.namespace('Collections').LockingMixin = function() {
  this.updating = false;

  this.beginUpdate = function() {
    this.updating = true;
  };

  this.isUpdating = function() {
    return this.updating;
  };

  this.onUpdated = function() {};

  this.endUpdate = function() {
    this.updating = false;
    this.onUpdated();
  };

  return this;
};
Dolphin.namespace('Collections').QueueMixin = function() {
  this.initFromArguments = function() {
    for (var i = 0; i < arguments.length; i++) {
      var item = arguments[i];
      if (item instanceof Array) {
        this.items = this.items.concat(item);
      } else {
        this.items.push(item);
      }
    }
  };

  this.enqueue = function(element) {
    this.items.push(element);
    return this;
  };

  this.dequeue = function() {
    return this.items.shift();
  };

  this.front = function() {
    if (this.isEmpty()) {
      throw new Dolphin.Exceptions.IndexOutOfRange();
    }
    return this.items[0];
  };

  return this;
};
Dolphin.namespace('Collections').List = function() {
  this.items = [];

  var _self = this;

  function addItem(item) {
    if (item instanceof Dolphin.Collections.List) {
      _self.append(item);
    } else {
      _self.items.push(item);
    }
  }

  for (var i = 0; i < arguments.length; i++) {
    var source = arguments[i];
    if (source instanceof Array) {
      source.forEach(
        function(item) {
          addItem(item);
        }.bind(this)
      );
    } else {
      addItem(source);
    }
  }
};

Dolphin.Collections.CollectionMixin.call(Dolphin.Collections.List.prototype);
Dolphin.Collections.ListMixin.call(Dolphin.Collections.List.prototype);
Dolphin.namespace('Collections').Stack = function() {
  this.items = [];
};

Dolphin.Collections.Stack.prototype = {
  push: function(item) {
    this.items.push(item);
    return this;
  },

  pop: function(item) {
    if (this.isEmpty()) {
      throw new Dolphin.Exceptions.IndexOutOfRange();
    }
    return this.items.pop();
  },

  peek: function() {
    if (this.isEmpty()) {
      throw new Dolphin.Exceptions.IndexOutOfRange();
    }
    return this.items[this.items.length - 1];
  }
};

Dolphin.Collections.CollectionMixin.call(Dolphin.Collections.Stack.prototype);
Dolphin.namespace('Collections').Queue = function() {
  this.items = [];
  this.initFromArguments.apply(this, arguments);
};

Dolphin.Collections.CollectionMixin.call(Dolphin.Collections.Queue.prototype);
Dolphin.Collections.QueueMixin.call(Dolphin.Collections.Queue.prototype);
Dolphin.namespace('Collections').QueueElement = function(element, priority) {
  this.element = element;
  this.priority = priority;
};

Dolphin.namespace('Collections').PriorityQueue = function() {
  this.items = [];
  this.initFromArguments.apply(this, arguments);
};

Dolphin.Collections.CollectionMixin.call(Dolphin.Collections.PriorityQueue.prototype);
Dolphin.Collections.QueueMixin.call(Dolphin.Collections.PriorityQueue.prototype);

Dolphin.Collections.PriorityQueue.prototype.enqueue = function(element, priority) {
  var queueElement = new Dolphin.Collections.QueueElement(element, priority);
  if (this.isEmpty()) {
    this.items.push(queueElement);
  } else {
    var added = false;
    for (var i = 0; i < this.items.length; i++) {
      if (queueElement.priority < this.items[i].priority) {
        this.items.splice(i, 0, queueElement);
        added = true;
        break;
      }
    }
    if (!added) {
      this.items.push(queueElement);
    }
  }
  return this;
};
