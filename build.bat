@ECHO OFF
SET MSBUILD_DIR="C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\"
forfiles /S /M *.csproj /c "cmd /c %%MSBUILD_DIR%%MSBuild @path /t:Clean,Build,Pack"
forfiles /S /M *.nuspec /c "cmd /c nuget pack @file -OutputDirectory ..\PackageOutput"