Dolphin.namespace('').LocalStorage = (function() {
  if (!window.localStorage || typeof localStorage.clear !== 'function') {
    throw 'The browser does not supprot window.localStorage';
  }

  function clear() {
    localStorage.clear();
  }

  function get(key) {
    var value = localStorage.getItem(key);
    return JSON.parse(value) || {};
  }

  function set(key, object) {
    localStorage.setItem(key, JSON.stringify(object));
  }

  return {
    clear: clear,
    get: get,
    set: set
  };
})();
