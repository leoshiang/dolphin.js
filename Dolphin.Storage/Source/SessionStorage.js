Dolphin.namespace('').SessionStorage = (function() {
  if (!window.sessionStorage || typeof sessionStorage.setItem !== 'function') {
    throw 'The browser does not support window.sessionStorage';
  }

  function clear() {
    sessionStorage.clear();
  }

  function get(key) {
    var value = sessionStorage.getItem(key);
    return JSON.parse(value) || {};
  }

  function set(key, object) {
    sessionStorage.setItem(key, JSON.stringify(object));
  }

  return {
    clear: clear,
    get: get,
    set: set
  };
})();
