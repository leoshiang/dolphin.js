# Dolphin.Collections.List

## constructor

以下為建立 List 基本方法：

```javascript
var list = new Dolphin.Collections.List();
```

你也可以傳入多個參數，每一個參數被視為一個元素：

```javascript
var list = new Dolphin.Collections.List(['a', 'b', 'c']);
```

也可以傳入多個陣列：

```javascript
var list = new Dolphin.Collections.List(['a', 'b'], ['c'], ['d', 'e']);
```

每一個參數可以是不同的形態：

```javascript
var list = new Dolphin.Collections.List('a', 1, new Date(), {});
```

如果傳入 List，將會複製 List 內容：

```javascript
var list1 = new Dolphin.Collections.List(1, 2, 3, 4);
var list2 = new Dolphin.Collections.List(list2);
console.log(list2.equal(list1)); // true
```

傳入 List 陣列

```javascript
var list1 = new Dolphin.Collections.List([1,2,3]);
var list2 = new Dolphin.Collections.List([4,5,6]);
var list3 = new Dolphin.Collections.List([list1, list2]);
```

## add

加入一個元素。

### 參數

#### item

要加入的元素，可以是任何型態的參數，例如：null, undefined, string。

> 如果傳入陣列，會將整個陣列視為單一元素加入 List 中。

### 回傳值

串列本身，型態為 Dolphin.Collections.List。

### 範例

```javascript
var list = new Dolphin.Collections.List();
list.add(1);
list.add('test');
list.add({});
list.add(['a', 'b']);
list.add(undefined);
list.add(null);
```

由於 add 會回傳 List 自己，因此上面的範例可以改寫為：

```javascript
var list = new Dolphin.Collections.List()
	.add(1);
	.add('test');
	.add({});
	.add(['a', 'b']);
	.add(undefined);
	.add(null);
```

如果呼叫 add 沒有傳入參數，將會加入 undefined：

```javascript
var list = new Dolphin.Collections.List().add();
consoloe.log(list.count()); // 1
consoloe.log(list.get(0)); // undefined
```

## append

append 方法不改變串列本身既有的資料，將傳入的 list 內容附加到串列中。

### 參數

#### list

要被加入的串列，型態為 Dolphin.Collections.List。

### 回傳值

串列本身，型態為 Dolphin.Collections.List。

### 例外

如果 list 型態不是 Dolphin.Collections.List，會拋出例外 TypeError。

如果 list 是 null 或 undefined，會拋出例外 Dolphin.Exceptions.NullException。

### 範例

```javascript
var list1 = new Dolphin.Collections.List([1,2,3]);
var list2 = new Dolphin.Collections.List([4,5,6]);
var list3 = new Dolphin.Collections.List();
list2.append(list1).append(list2);
```

## assign

以參數 list 的內容取代掉串列本身的內容。

### 參數

#### list

來源串列，型態為 Dolphin.Collections.List。

### 回傳值

串列本身，型態為 Dolphin.Collections.List。

### 例外

如果 list 型態不是 Dolphin.Collections.List，會拋出例外 TypeError。

如果 list 是 null 或 undefined，會拋出例外 Dolphin.Exceptions.NullException。

### 範例

```javascript
var list1 = new Dolphin.Collections.List([1,2,3]);
var list2 = new Dolphin.Collections.List();
list2.assign(list1);
```

## clear

清除內容，長度變為 0。

### 參數

無

### 回傳值

串列本身，型態為 Dolphin.Collections.List。

### 範例

```javascript
var list = new Dolphin.Collections.List([1, 2, 3, 4]);
list.clear();
console.log(list.count()); // 0
```

### 備註

>  clear 並非 Dolphin.Collections.List 本身的 function，而是來自 CollectionMixin。

## clone

建立新的實體並將內容複製過去。

### 參數

無

### 回傳值

新的 Dolphin.Collections.List 實體 。

### 範例

```javascript
var list1 = new Dolphin.Collections.List([1, 2, 3, 4]);
var list2 = list1.clone();
console.log(list2.count()) // 4
```

## count

傳回串列元素的數目。

### 參數

無

### 回傳值

元素的數目 。

### 範例

```javascript
var list = new Dolphin.Collections.List([1, 2, 3, 4]);
console.log(list.count()); // 4
```

### 備註

> count 並非 Dolphin.Collections.List 本身的 function，而是來自 CollectionMixin。

## delete

刪除特定位置的元素。

### 參數

#### index

索引值，從 0 開始。

### 回傳值

串列本身，型態為 Dolphin.Collections.List。

### 範例

```javascript
var list = new Dolphin.Collections.List([1, 2, 3, 4]);
list.delete(0);
list.delete(2);
list.delete(3); // IndexOutOfRange
```

也可以使用 method chaining 

```javascript
var list = new Dolphin.Collections.List([1, 2, 3, 4]);
list.delete(0).delete(2).delete(3); // IndexOutOfRange
```

### 例外

如果索引值超出範圍，則會拋出例外 IndexOutOfRange。

## difference

傳回與參數 list 的差集運算結果。以下圖為例，A 串列的元素扣掉也出現在B串列的元素即為差集運算。

![List.difference](List.assets/List.difference.png)

### 參數

#### list

來源串列，型態為 Dolphin.Collections.List。

### 回傳值

新的 Dolphin.Collections.List 實體 。

### 範例

```javascript
var listA = new Dolphin.Collections.List([1,2,3]);
var listB = new Dolphin.Collections.List([2,3,4,5,6]);
var difference = listA.difference(listB); // [1]
```

### 例外

如果 list 型態不是 Dolphin.Collections.List，則會拋出例外 TypeError。

如果 list 是 null 或是 undefined，則會拋出例外 Dolphin.Exceptions.NullException。

### 備註

>  使用 Dolphin.deepEqual 比較物件的每一個屬性質是否相同。

## distinct

傳回串列中不重複的元素，並建立新的 List 實體。

### 參數

無

### 回傳值

新的 Dolphin.Collections.List 實體 。

### 範例

```javascript
var list = new Dolphin.Collections.List([1,2,3,3,2,2,2,1,1,4]);
var distinct = list.distinct(); // [1,3,2,4]
```

### 備註

> 使用 Dolphin.deepEqual 比較物件的每一個屬性質是否相同。

## equal

比較串列內容是否相同。

### 參數

####  source

資料來源 source 可以是 Dolphin.Collections.List 也可以是陣列。

equal 函式會比較長度是否相同，接著再比較每個元素是否相同。

### 回傳值

True 代表每一個元素皆相同，False 代表長度或是有一個以上的元素不相同。

### 範例

```javascript
var list1 = new Dolphin.Collections.List(['a', 'b', 'c']);
var list2 = new Dolphin.Collections.List(['a', 'b', 'c']);
console.log(list1.equal(list2)) // true
```

如果物件屬性不同，結果會是 false

```javascript
var list1 = new Dolphin.Collections.List([{code: 1}]);
var list2 = new Dolphin.Collections.List([{name: 'a'}]);
console.log(list1.equal(list2)) // false
```

### 例外

如果 source 不是陣列或 Dolphin.Collections.List 會拋出例外 TypeError。

如果 source 是 null 或是 undefined 會拋出例外 Dolphin.Exceptions.NullException。

### 備註

> 使用 Dolphin.deepEqual 比較物件的每一個屬性質是否相同。

## every

測試每一個元素是否通過給定的函式的測試。

### 參數

#### callback

回呼函數，形式為

```javascript
function(element, index) {
    
};
```

### 回傳值

True 代表每一個元素通過給定的函式的測試，False 代表每一個元素沒通過給定的函式的測試。

### 例外

如果不是 function 型態，會拋出例外 TypeError。

### 範例

```javascript
function isBigEnough(element, index) {
    return element >= 10;
}

var list1 = new Dolphin.Collections.List([12, 5, 8, 130, 44]);
console.log(list1.every(isBigEnough)); // false

var list2 = new Dolphin.Collections.List([12, 54, 18, 130, 44]);
console.log(list2.every(isBigEnough)); // true
```

## exchange

## filter

## forEach

## get

## indexOf

## indexOfItem

## intersection

## move

## put

## remove

## reverse

## sort

## subset

## union

傳回與參數 list 的聯集運算結果。以下圖為例，A 串列的元素扣掉也出現在B串列的元素即為差集運算。

![List.unoin](List.assets/List.unoin.png)

### 參數

#### list

來源串列，型態為 Dolphin.Collections.List。

### 回傳值

新的 Dolphin.Collections.List 實體 。

### 範例

```javascript
var listA = new Dolphin.Collections.List([1,3,5,5]);
var listB = new Dolphin.Collections.List([2,4,4,6]);
var union = listA.union(listB); // [1,3,5,2,4,6]
```

### 例外

如果 list 型態不是 Dolphin.Collections.List，則會拋出例外 TypeError。

如果 list 是 null 或是 undefined，則會拋出例外 Dolphin.Exceptions.NullException。

### 備註

> 使用 Dolphin.deepEqual 比較物件的每一個屬性質是否相同。