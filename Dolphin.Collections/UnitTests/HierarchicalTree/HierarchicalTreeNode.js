﻿QUnit.module('Dolphin.Collections.HierarchicalTreeNode');

QUnit.test('測試 HierarchicalTreeNode 成員有被定義', function(assert) {
  assert.equal(typeof Dolphin.Collections.HierarchicalTreeNode, 'function');
  assert.equal(typeof Dolphin.Collections.HierarchicalTreeNode.prototype.addChild, 'function');
  assert.equal(typeof Dolphin.Collections.HierarchicalTreeNode.prototype.count, 'function');
  assert.equal(typeof Dolphin.Collections.HierarchicalTreeNode.prototype.findChild, 'function');
  assert.equal(typeof Dolphin.Collections.HierarchicalTreeNode.prototype.findChildById, 'function');
});

QUnit.test('Constructor', function(assert) {
  var node = new Dolphin.Collections.HierarchicalTreeNode(1);
  assert.equal(node.id, 1);
  assert.equal(node.children.count(), 0);
});
