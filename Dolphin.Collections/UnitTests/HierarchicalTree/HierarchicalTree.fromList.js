﻿QUnit.module('Dolphin.Collections.HierarchicalTree.fromList');

QUnit.test('重建層級樹', function(assert) {
  var tree = new Dolphin.Collections.HierarchicalTree(0);
  var flatTree = [
    { id: '1', parentId: '0', name: 'item1' },
    { id: '2', parentId: '0', name: 'item2' },
    { id: '3', parentId: '1', name: 'item3' },
    { id: '4', parentId: '2', name: 'item4' }
  ];
  var list = new Dolphin.Collections.List(flatTree);
  var options = {
    idKey: 'id',
    parentKey: 'parentId',
    rootId: '0'
  };
  tree.fromList(list, options);
  assert.equal(tree.count(), 5);
});
