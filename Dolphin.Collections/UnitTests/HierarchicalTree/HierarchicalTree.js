﻿QUnit.module('Dolphin.Collections.HierarchicalTree');

QUnit.test('測試 HierarchicalTree 成員有被定義', function(assert) {
  assert.equal(typeof Dolphin.Collections.HierarchicalTree, 'function');
  assert.equal(typeof Dolphin.Collections.HierarchicalTree.prototype.fromList, 'function');
});

QUnit.test('Constructor', function(assert) {
  var tree = new Dolphin.Collections.HierarchicalTree(0);
  assert.ok(tree !== undefined);
  assert.ok(tree.root !== undefined);
});
