﻿QUnit.module('Dolphin.Collections.HierarchicalTreeNode.findChildById');

QUnit.test('加入node，應能用id找到', function(assert) {
  var root = new Dolphin.Collections.HierarchicalTreeNode(1);
  var child = new Dolphin.Collections.HierarchicalTreeNode(2);
  root.addChild(child);
  var found = root.findChildById(2);
  assert.equal(found.id, 2);
});
