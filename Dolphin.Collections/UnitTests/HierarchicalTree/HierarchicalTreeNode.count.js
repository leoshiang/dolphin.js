﻿QUnit.module('Dolphin.Collections.HierarchicalTreeNode.count');

QUnit.test('加入一個 node ， children 數目應等於 1', function(assert) {
  var root = new Dolphin.Collections.HierarchicalTreeNode(1);
  var child = new Dolphin.Collections.HierarchicalTreeNode(2);
  root.addChild(child);
  assert.equal(root.count(), 1);
});
