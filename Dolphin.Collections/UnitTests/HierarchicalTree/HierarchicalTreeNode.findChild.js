﻿QUnit.module('Dolphin.Collections.HierarchicalTreeNode.findChild');

QUnit.test('加入node，應能找到', function(assert) {
  var root = new Dolphin.Collections.HierarchicalTreeNode(1);
  var child = new Dolphin.Collections.HierarchicalTreeNode(2);
  root.addChild(child);
  var found = root.findChild(function(child) {
    return child.id === 2;
  });
  assert.equal(found.id, 2);
});
