﻿QUnit.module('Dolphin.Collections.PriorityQueue.isEmpty');

QUnit.test('無項目，應回傳 true', function(assert) {
  var queue = new Dolphin.Collections.PriorityQueue();
  assert.ok(queue.isEmpty());
});

QUnit.test('加入 1 個項目，應回傳 false', function(assert) {
  var queue = new Dolphin.Collections.PriorityQueue();
  queue.enqueue(1, 1);
  assert.notOk(queue.isEmpty());
});
