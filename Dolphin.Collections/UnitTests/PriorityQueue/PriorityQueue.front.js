﻿QUnit.module('Dolphin.Collections.PriorityQueue.front');

QUnit.test('加入 1 個項目，應等於加入的項目', function(assert) {
  var queue = new Dolphin.Collections.PriorityQueue();
  queue.enqueue(1, 1);
  assert.equal(queue.front().element, 1);
});
