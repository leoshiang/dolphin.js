﻿QUnit.module('Dolphin.Collections.PriorityQueue');

QUnit.test('確認成員有被定義', function(assert) {
  assert.equal(typeof Dolphin.Collections.PriorityQueue, 'function');
  assert.equal(typeof Dolphin.Collections.PriorityQueue.prototype.clear, 'function');
  assert.equal(typeof Dolphin.Collections.PriorityQueue.prototype.count, 'function');
  assert.equal(typeof Dolphin.Collections.PriorityQueue.prototype.isEmpty, 'function');
  assert.equal(typeof Dolphin.Collections.PriorityQueue.prototype.enqueue, 'function');
  assert.equal(typeof Dolphin.Collections.PriorityQueue.prototype.dequeue, 'function');
  assert.equal(typeof Dolphin.Collections.PriorityQueue.prototype.front, 'function');
});

QUnit.test('呼叫建構子，應建立物件', function(assert) {
  var queue = new Dolphin.Collections.Queue();
  assert.equal(typeof queue, 'object');
});

function iniQueueElement() {
  var random = Math.floor(Math.random() * 10 + 1);
  return Dolphin.Collections.QueueElement(random, random);
}

QUnit.test('呼叫建構子傳入多個引數，應將每個引數加入', function(assert) {
  var queue = new Dolphin.Collections.PriorityQueue([
    iniQueueElement(),
    iniQueueElement(),
    iniQueueElement(),
    iniQueueElement()
  ]);
  assert.equal(queue.count(), 4);
});

QUnit.test('呼叫建構子傳入多個陣列引數，應將陣列的每個項目加入', function(assert) {
  var queue = new Dolphin.Collections.PriorityQueue(
    [iniQueueElement(), iniQueueElement(), iniQueueElement(), iniQueueElement()],
    [iniQueueElement()],
    [iniQueueElement(), iniQueueElement()]
  );
  assert.equal(queue.count(), 7);
});
