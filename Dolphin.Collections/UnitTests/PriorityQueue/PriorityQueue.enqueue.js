﻿QUnit.module('Dolphin.Collections.PriorityQueue.enqueue');

QUnit.test('加入4個資料，數量應等於4且順序應該照優先度排列', function(assert) {
  var queue = new Dolphin.Collections.PriorityQueue();
  queue.enqueue('APPLE', 2);
  queue.enqueue('GOOGLE', 1);
  queue.enqueue('MICROSOFT', 3);
  queue.enqueue('ORACLE', 1);
  assert.equal(queue.count(), 4);
  assert.equal(queue.dequeue().element, 'GOOGLE');
  assert.equal(queue.dequeue().element, 'ORACLE');
  assert.equal(queue.dequeue().element, 'APPLE');
  assert.equal(queue.dequeue().element, 'MICROSOFT');
});
