﻿QUnit.module('Dolphin.Collections.PriorityQueue.count');

QUnit.test('無項目，應回傳 0', function(assert) {
  var queue = new Dolphin.Collections.PriorityQueue();
  assert.equal(queue.count(), 0);
});

QUnit.test('加入 1 個項目，應回傳 1', function(assert) {
  var queue = new Dolphin.Collections.PriorityQueue();
  queue.enqueue(1, 1);
  assert.equal(queue.count(), 1);
});
