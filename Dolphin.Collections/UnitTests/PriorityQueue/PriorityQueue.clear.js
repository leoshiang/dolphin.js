﻿QUnit.module('Dolphin.Collections.PriorityQueue.clear');

QUnit.test('無項目，數量應回傳 0', function(assert) {
  var queue = new Dolphin.Collections.PriorityQueue();
  queue.clear();
  assert.equal(queue.count(), 0);
});

QUnit.test('加入 1 個項目，數量應回傳 0', function(assert) {
  var queue = new Dolphin.Collections.PriorityQueue();
  queue.enqueue(1, 1);
  queue.clear();
  assert.equal(queue.count(), 0);
});
