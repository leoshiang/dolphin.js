﻿QUnit.module('Dolphin.Collections.List.sort');

QUnit.test('[3,2,1] 按升冪排序，應改為 [1,2,3]', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(3);
  list.add(2);
  list.add(1);
  list.sort(function(a, b) {
    if (a < b) {
      return -1;
    } else if (a > b) {
      return 1;
    } else {
      return 0;
    }
  });

  assert.equal(list.get(0), 1);
  assert.equal(list.get(1), 2);
  assert.equal(list.get(2), 3);
});

QUnit.test("'[1,2,3] 按降冪排序，應改為 [3,2,1]", function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(1);
  list.add(2);
  list.add(3);
  list.sort(function(a, b) {
    if (a < b) {
      return 1;
    } else if (a > b) {
      return -1;
    } else {
      return 0;
    }
  });

  assert.equal(list.get(0), 3);
  assert.equal(list.get(1), 2);
  assert.equal(list.get(2), 1);
});
