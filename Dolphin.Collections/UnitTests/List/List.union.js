﻿QUnit.module('Dolphin.Collections.List.union');

QUnit.test('兩個空 list，應回傳空', function(assert) {
  var list1 = new Dolphin.Collections.List();
  var list2 = new Dolphin.Collections.List();
  var list3 = list1.union(list2);

  assert.equal(list3.count(), 0);
});

QUnit.test('兩個不同 list，應回傳兩個list的內容', function(assert) {
  var list1 = new Dolphin.Collections.List([1, 3, 5, 5]);
  var list2 = new Dolphin.Collections.List([2, 4, 4, 6]);
  var list3 = list1.union(list2);

  assert.equal(list3.count(), 6);
  assert.equal(list3.get(0), 1);
  assert.equal(list3.get(1), 3);
  assert.equal(list3.get(2), 5);
  assert.equal(list3.get(3), 2);
  assert.equal(list3.get(4), 4);
  assert.equal(list3.get(5), 6);
});

QUnit.test('兩個list其中一個為空，應回傳第一個list的內容', function(assert) {
  var list1 = new Dolphin.Collections.List([1, 3, 5]);
  var list2 = new Dolphin.Collections.List([]);
  var list3 = list1.union(list2);

  assert.equal(list3.count(), 3);
  assert.equal(list3.get(0), 1);
  assert.equal(list3.get(1), 3);
  assert.equal(list3.get(2), 5);
});

QUnit.test('兩個list其中一個為空，應回傳第一個list的內容', function(assert) {
  var list1 = new Dolphin.Collections.List([]);
  var list2 = new Dolphin.Collections.List([1, 3, 5]);
  var list3 = list1.union(list2);

  assert.equal(list3.count(), 3);
  assert.equal(list3.get(0), 1);
  assert.equal(list3.get(1), 3);
  assert.equal(list3.get(2), 5);
});
