﻿QUnit.module('Dolphin.Collections.List.subset');

QUnit.test('[1,2] 是 [1,2,3] 的子集合', function(assert) {
  var list1 = new Dolphin.Collections.List([1, 2]);
  var list2 = new Dolphin.Collections.List([1, 2, 3]);
  assert.ok(list1.subset(list2));
});

QUnit.test('[1,2,3] 不是 [1,2] 的子集合', function(assert) {
  var list1 = new Dolphin.Collections.List([1, 2, 3]);
  var list2 = new Dolphin.Collections.List([1, 2]);
  assert.notOk(list1.subset(list2));
});
