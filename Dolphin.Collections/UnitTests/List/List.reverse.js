﻿QUnit.module('Dolphin.Collections.List.reverse');

QUnit.test('[1,2, 3] => [3,2,1]', function(assert) {
  var list = new Dolphin.Collections.List([1, 2, 3]);
  list.reverse();
  assert.equal(list.count(), 3);
  assert.equal(list.get(0), 3);
  assert.equal(list.get(1), 2);
  assert.equal(list.get(2), 1);
});
