﻿QUnit.module('Dolphin.Collections.List.clear');

QUnit.test('無資料，清除之後長度應等於 0', function(assert) {
  var list = new Dolphin.Collections.List();
  list.clear();
  assert.equal(list.count(), 0);
});

QUnit.test('加入 1 個項目，清除之後長度應等於 0', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(999);
  list.clear();
  assert.equal(list.count(), 0);
});
