﻿QUnit.module('Dolphin.Collections.List.remove');

QUnit.test('無項目，移除資料 a ，應回傳 false', function(assert) {
  var list = new Dolphin.Collections.List();
  assert.equal(list.remove('a'), false);
});

QUnit.test('[a,b,c] 移除資料 d ，應回傳 false', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add('a');
  list.add('b');
  list.add('c');
  assert.equal(list.remove('d'), false);
  assert.equal(list.count(), 3);
});

QUnit.test('串列 [a] 移除資料 a ，應回傳 true', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add('a');
  assert.equal(list.remove('a'), true);
  assert.equal(list.count(), 0);
});

QUnit.test('[a,b,c] 移除資料 a ，應回傳 true', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add('a');
  list.add('b');
  list.add('c');
  assert.equal(list.remove('b'), true);
  assert.equal(list.count(), 2);
});
