﻿QUnit.module('Dolphin.Collections.List.clone');

QUnit.test('複製空 List，結果的長度應為 0', function(assert) {
  var list = new Dolphin.Collections.List().clone();
  assert.equal(list.count(), 0);
});

QUnit.test('複製有資料的 List ，結果的內容應相同', function(assert) {
  var list1 = new Dolphin.Collections.List([1, 2, 3, 4]);
  var list2 = list1.clone();
  assert.ok(list2.equal(list1));
});
