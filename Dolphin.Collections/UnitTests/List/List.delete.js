﻿QUnit.module('Dolphin.Collections.List.delete');

QUnit.test('無項目，刪除應拋出例外 Dolphin.Exceptions.IndexOutOfRange', function(assert) {
  var list = new Dolphin.Collections.List();
  assert.throws(
    function() {
      list.delete(0);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.IndexOutOfRange;
    }
  );
});

QUnit.test('加入 3 個項目然後刪除中間的，應保留原本頭尾的項目', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(1);
  list.add(2);
  list.add(3);
  list.delete(1);
  assert.equal(list.get(0), 1);
  assert.equal(list.get(1), 3);
});

QUnit.test('加入 3 個項目然後刪除第 1 個，應保留後 2 個項目', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(1);
  list.add(2);
  list.add(3);
  list.delete(0);
  assert.equal(list.get(0), 2);
  assert.equal(list.get(1), 3);
});

QUnit.test('加入 3 個項目然後刪除最後 1 個，應保留前 2 個項目', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(1);
  list.add(2);
  list.add(3);
  list.delete(2);
  assert.equal(list.get(0), 1);
  assert.equal(list.get(1), 2);
});
