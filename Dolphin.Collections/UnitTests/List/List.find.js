﻿QUnit.module('Dolphin.Collections.List.find');

QUnit.test('callback 傳入 null，應拋出 TypeError', function(assert) {
  assert.throws(
    function() {
      var list = new Dolphin.Collections.List();
      list.find(null);
    },
    function(error) {
      return error instanceof TypeError;
    }
  );
});

QUnit.test('callback 傳入 object，應拋出 TypeError', function(assert) {
  assert.throws(
    function() {
      var list = new Dolphin.Collections.List();
      list.find({});
    },
    function(error) {
      return error instanceof TypeError;
    }
  );
});

QUnit.test('無項目，應回傳 undefined', function(assert) {
  function isStringA(item) {
    return item === 'a';
  }

  var list = new Dolphin.Collections.List();
  assert.equal(list.find(isStringA), undefined);
});

QUnit.test('有資料，應回傳該物件', function(assert) {
  function isStringC(item) {
    return item === 'c';
  }

  var list = new Dolphin.Collections.List();
  list.add('a');
  list.add('b');
  list.add('c');
  assert.equal(list.find(isStringC), 'c');
});
