﻿QUnit.module('Dolphin.Collections.List.put');

QUnit.test('無項目，放位置 1 應拋出例外 Dolphin.Exceptions.IndexOutOfRange', function(assert) {
  var list = new Dolphin.Collections.List();
  assert.throws(
    function() {
      list.put(1);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.IndexOutOfRange;
    }
  );
});

QUnit.test('無項目，放位置 -1 應拋出例外 Dolphin.Exceptions.IndexOutOfRange', function(assert) {
  var list = new Dolphin.Collections.List();
  assert.throws(
    function() {
      list.put(-1);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.IndexOutOfRange;
    }
  );
});

QUnit.test('串列 [1,2,3] 把 [4] 放到位置 2，串列應改為 [1,2,4]', function(assert) {
  var list = new Dolphin.Collections.List([1, 2, 3]).put(2, 4);
  assert.ok(list.equal(new Dolphin.Collections.List([1, 2, 4])));
});
