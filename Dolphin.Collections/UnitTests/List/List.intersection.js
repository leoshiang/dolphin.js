﻿QUnit.module('Dolphin.Collections.List.intersection');

QUnit.test('兩個空 list，應回傳空 list', function(assert) {
  var list1 = new Dolphin.Collections.List();
  var list2 = new Dolphin.Collections.List();
  var list3 = list1.intersection(list2);

  assert.equal(list3.count(), 0);
});

QUnit.test('[1,2,3] 交集 [3,4,5,6]，應回傳 [3]', function(assert) {
  var list1 = new Dolphin.Collections.List([1, 2, 3]);
  var list2 = new Dolphin.Collections.List([3, 4, 5, 6]);
  var list3 = list1.intersection(list2);

  assert.equal(list3.count(), 1);
  assert.equal(list3.get(0), 3);
});

QUnit.test('[1,2,3] 交集 [2,3,4,5,6]，應回傳 [2,3]', function(assert) {
  var list1 = new Dolphin.Collections.List([1, 2, 3]);
  var list2 = new Dolphin.Collections.List([2, 3, 4, 5, 6]);
  var list3 = list1.intersection(list2);

  assert.equal(list3.count(), 2);
  assert.equal(list3.get(0), 2);
  assert.equal(list3.get(1), 3);
});
