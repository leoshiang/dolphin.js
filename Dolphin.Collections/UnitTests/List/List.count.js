﻿QUnit.module('Dolphin.Collections.List.count');

QUnit.test('無項目，應回傳 0', function(assert) {
  var list = new Dolphin.Collections.List();
  assert.equal(list.count(), 0);
});

QUnit.test('加入 3 個項目，應回傳 3', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(1);
  list.add(2);
  list.add(3);
  assert.equal(list.count(), 3);
});
