﻿QUnit.module('Dolphin.Collections.List.assign');

QUnit.test('指派 null ，應拋出例外 Dolphin.Exceptions.NullException', function(assert) {
  var list = new Dolphin.Collections.List();
  assert.throws(
    function() {
      list.assign(null);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.NullException;
    }
  );
});

QUnit.test('指派 undefined ，應拋出例外 Dolphin.Exceptions.NullException', function(assert) {
  var list = new Dolphin.Collections.List();
  assert.throws(
    function() {
      list.assign(null);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.NullException;
    }
  );
});

QUnit.test('指派非 List 型態，應拋出例外 TypeError', function(assert) {
  function assign(type) {
    var list = new Dolphin.Collections.List();
    assert.throws(
      function() {
        list.assign(type);
      },
      function(error) {
        return error instanceof TypeError;
      }
    );
  }

  assign({});
  assign(1);
  assign('');
  assign(function() {});
});

QUnit.test('指派 List ，內容應被取代', function(assert) {
  var list1 = new Dolphin.Collections.List([4, 5, 6]);
  var list2 = new Dolphin.Collections.List([1, 2, 3]);
  list2.assign(list1);
  assert.equal(list2.count(), 3);
  assert.equal(list2.get(0), 4);
  assert.equal(list2.get(1), 5);
  assert.equal(list2.get(2), 6);
});
