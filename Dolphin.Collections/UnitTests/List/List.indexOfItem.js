﻿function simpleObject(index) {
  return {
    code: index
  };
}

function complexObject(index) {
  return {
    code: index,
    info: {
      name: index * index
    }
  };
}

QUnit.module('Dolphin.Collections.List.indexOfItem');

QUnit.test('加入三個簡單物件，目標位置應是在 1', function(assert) {
  var list = new Dolphin.Collections.List()
    .add(simpleObject(1))
    .add(simpleObject(2))
    .add(simpleObject(3));

  assert.equal(list.indexOfItem({ code: 2 }), 1);
});

QUnit.test('加入三個簡單物件，尋找不存在的項目應回傳 -1', function(assert) {
  var list = new Dolphin.Collections.List()
    .add(simpleObject(1))
    .add(simpleObject(2))
    .add(simpleObject(3));

  assert.equal(list.indexOfItem({ code: 9 }), -1);
});

QUnit.test('加入三個複雜物件，目標位置應是在 2', function(assert) {
  var list = new Dolphin.Collections.List()
    .add(complexObject(1))
    .add(complexObject(2))
    .add(complexObject(3));

  var target = {
    code: 3,
    info: {
      name: 9
    }
  };

  assert.equal(list.indexOfItem(target), 2);
});

QUnit.test('加入二個複雜物件、一個簡單物件，目標位置應是在 1', function(assert) {
  var list = new Dolphin.Collections.List()
    .add(complexObject(1))
    .add(simpleObject(2))
    .add(complexObject(3));

  var target = {
    code: 2
  };

  assert.equal(list.indexOfItem(target), 1);
});

QUnit.test('加入字串，目標位置應該是在 2', function(assert) {
  var list = new Dolphin.Collections.List()
    .add({ name: 'BMW' })
    .add({ name: 'BENZ' })
    .add({ name: 'VOLVO' })
    .add({ name: 'NISSAN' });
  var index = list.indexOfItem({ name: 'VOLVO' });
  assert.equal(index, 2);
});

QUnit.test('加入字串與null，目標位置應該是在 2', function(assert) {
  var list = new Dolphin.Collections.List()
    .add(null)
    .add(null)
    .add({ name: 'VOLVO' })
    .add(null);
  var index = list.indexOfItem({ name: 'VOLVO' });
  assert.equal(index, 2);
});
