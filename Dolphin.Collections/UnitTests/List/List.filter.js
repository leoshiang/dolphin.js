﻿QUnit.module('Dolphin.Collections.List.filter');

QUnit.test('[12, 5, 8, 130, 44] 過濾大於 10 的項目，應回傳 [12,130,44] 的 List', function(assert) {
  function isBigEnough(value) {
    return value >= 10;
  }

  var list = new Dolphin.Collections.List([12, 5, 8, 130, 44]);
  var filtered = list.filter(isBigEnough);
  assert.equal(filtered.count(), 3);
  assert.equal(filtered.get(0), 12);
  assert.equal(filtered.get(1), 130);
  assert.equal(filtered.get(2), 44);
});

QUnit.test('[12, 5, 8, 130, 44] 過濾小於 1 的項目，應回傳 [] 的 List', function(assert) {
  function isSmallEnough(value) {
    return value < 1;
  }

  var list = new Dolphin.Collections.List([12, 5, 8, 130, 44]);
  var filtered = list.filter(isSmallEnough);
  assert.equal(filtered.count(), 0);
});
