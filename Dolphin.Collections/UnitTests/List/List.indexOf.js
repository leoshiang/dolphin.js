﻿QUnit.module('Dolphin.Collections.List.indexOf');

QUnit.test('無項目，應回傳 -1', function(assert) {
  var list = new Dolphin.Collections.List();
  assert.equal(list.indexOf('a'), -1);
});

QUnit.test('串列內容是 [a] ，傳入 a 應回傳 0', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add('a');
  assert.equal(list.indexOf('a'), 0);
});

QUnit.test('串列內容是 [a] ，傳入 b 應回傳 -1', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add('a');
  assert.equal(list.indexOf('b'), -1);
});

QUnit.test('串列內容是 [a,b,b] ，傳入 b 應回傳 1', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add('a');
  list.add('b');
  list.add('b');
  assert.equal(list.indexOf('b'), 1);
});
