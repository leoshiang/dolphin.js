﻿QUnit.module('Dolphin.Collections.List.distinct');

QUnit.test('[1,2,3,3,2,2,2,1,1,4]，應回傳 [1,2,3,4]', function(assert) {
  var list = new Dolphin.Collections.List([1, 2, 3, 3, 2, 2, 2, 1, 1, 4]);
  var distinct = list.distinct();
  assert.equal(distinct.count(), 4);
  assert.equal(distinct.get(0), 1);
  assert.equal(distinct.get(1), 2);
  assert.equal(distinct.get(2), 3);
  assert.equal(distinct.get(3), 4);
});
