﻿QUnit.module('Dolphin.Collections.List.difference');

QUnit.test('[1,2,3] 差集 [2,3,4,5,6]，應回傳[1]', function(assert) {
  var list1 = new Dolphin.Collections.List([1, 2, 3]);
  var list2 = new Dolphin.Collections.List([2, 3, 4, 5, 6]);
  var list3 = list1.difference(list2);

  assert.equal(list3.count(), 1);
  assert.equal(list3.get(0), 1);
});

QUnit.test('[2,3,4,5,6] 差集 [1,2,3] ，應回傳[4,5,6]', function(assert) {
  var list1 = new Dolphin.Collections.List([2, 3, 4, 5, 6]);
  var list2 = new Dolphin.Collections.List([1, 2, 3]);
  var list3 = list1.difference(list2);

  assert.equal(list3.count(), 3);
  assert.equal(list3.get(0), 4);
  assert.equal(list3.get(1), 5);
  assert.equal(list3.get(2), 6);
});

QUnit.test('[] 差集 []，應回傳[]', function(assert) {
  var list1 = new Dolphin.Collections.List([]);
  var list2 = new Dolphin.Collections.List([]);
  var list3 = list1.difference(list2);

  assert.equal(list3.count(), 0);
});
