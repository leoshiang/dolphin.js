﻿QUnit.module('Dolphin.Collections.List.add');

QUnit.test('無參數，應加入 1 個 undefined 的項目', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add();
  assert.equal(list.get(0), undefined);
});

QUnit.test('加入 1 個項目，第 0 個項目應為加入的項目', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(999);
  assert.equal(list.count(), 1);
});

QUnit.test('加入 undefined，第 0 個項目應為 undefined', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(undefined);
  assert.equal(list.get(0), undefined);
});

QUnit.test('加入 null，第 0 個項目應為 null', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(null);
  assert.equal(list.get(0), null);
});

QUnit.test('使用方法鏈加入三個項目，長度應等於 3', function(assert) {
  var list = new Dolphin.Collections.List()
    .add(1)
    .add(2)
    .add(3);
  assert.equal(list.count(), 3);
});
