﻿QUnit.module('Dolphin.Collections.List.map');
QUnit.test('callback 傳入 null，應拋出 TypeError', function(assert) {
  assert.throws(
    function() {
      var list = new Dolphin.Collections.List();
      list.map(null);
    },
    function(error) {
      return error instanceof TypeError;
    }
  );
});

QUnit.test('callback 傳入 object，應拋出 TypeError', function(assert) {
  assert.throws(
    function() {
      var list = new Dolphin.Collections.List();
      list.map({});
    },
    function(error) {
      return error instanceof TypeError;
    }
  );
});

QUnit.test('[1, 4, 9, 16] 每一個項目 * 2，應回傳 [2, 8, 18, 32]', function(assert) {
  var list = new Dolphin.Collections.List([1, 4, 9, 16]);
  var result = list.map(function(x) {
    return x * 2;
  });

  assert.equal(result.count(), 4);
  assert.equal(result.get(0), 2);
  assert.equal(result.get(1), 8);
  assert.equal(result.get(2), 18);
  assert.equal(result.get(3), 32);
});

QUnit.test('物件屬性轉換，應能正確轉換', function(assert) {
  function createObject(index) {
    return {
      code: index,
      name: index
    };
  }

  var list = new Dolphin.Collections.List()
    .add(createObject(1))
    .add(createObject(2))
    .add(createObject(3))
    .add(createObject(4));

  var result = list.map(function(x) {
    return {
      index: x.code,
      code: x.code * 2
    };
  });

  function createObject2(index) {
    return {
      index: index,
      code: index * 2
    };
  }

  var expected = new Dolphin.Collections.List()
    .add(createObject2(1))
    .add(createObject2(2))
    .add(createObject2(3))
    .add(createObject2(4));

  assert.ok(result.equal(expected));
});
