﻿QUnit.module('Dolphin.Collections.List.equal');

QUnit.test('傳入陣列與 List 之外的物件，應拋出例外 TypeError', function(assert) {
  function testEqual(x) {
    assert.throws(
      function() {
        list.equal(x);
      },
      function(error) {
        return error instanceof TypeError;
      }
    );
  }

  var list = new Dolphin.Collections.List();
  assert.expect(6);
  testEqual({});
  testEqual(function() {});
  testEqual(1);
  testEqual('');

  assert.throws(
    function() {
      list.equal(null);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.NullException;
    }
  );

  assert.throws(
    function() {
      list.equal(undefined);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.NullException;
    }
  );
});

QUnit.test('傳入兩個空 list，應回傳 true', function(assert) {
  var list1 = new Dolphin.Collections.List();
  var list2 = new Dolphin.Collections.List();

  assert.ok(list1.equal(list2));
});

QUnit.test('傳入兩個相同整數 list，應回傳 true', function(assert) {
  var list1 = new Dolphin.Collections.List([1, 2, 3]);
  var list2 = new Dolphin.Collections.List([1, 2, 3]);

  assert.ok(list1.equal(list2));
});

QUnit.test('傳入兩個相同字串 list，應回傳 true', function(assert) {
  var list1 = new Dolphin.Collections.List(['a', 'b', 'c']);
  var list2 = new Dolphin.Collections.List(['a', 'b', 'c']);

  assert.ok(list1.equal(list2));
});

QUnit.test('傳入兩個相同物件 list，應回傳 true', function(assert) {
  var list1 = new Dolphin.Collections.List([{}, {}, {}]);
  var list2 = new Dolphin.Collections.List([{}, {}, {}]);

  assert.ok(list1.equal(list2));
});

QUnit.test('傳入兩個不相同字串 list，應回傳 false', function(assert) {
  var list1 = new Dolphin.Collections.List(['a', 'b', 'c']);
  var list2 = new Dolphin.Collections.List(['a', 'c']);

  assert.notOk(list1.equal(list2));
});

QUnit.test('傳入物件屬性不同，應回傳 false', function(assert) {
  var list1 = new Dolphin.Collections.List([{ code: 1 }]);
  var list2 = new Dolphin.Collections.List([{ name: 'a' }]);

  assert.notOk(list1.equal(list2));
});
