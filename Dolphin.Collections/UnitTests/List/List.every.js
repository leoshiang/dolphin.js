﻿QUnit.module('Dolphin.Collections.List.every');

QUnit.test('callback 傳入非 function ，應拋出例外', function(assert) {
  var list = new Dolphin.Collections.List([12, 5, 8, 130, 44]);
  assert.throws(
    function() {
      list.equal({});
    },
    function(error) {
      return error instanceof TypeError;
    }
  );
});

QUnit.test('傳入 [12, 5, 8, 130, 44]，應判斷不是全部都 >= 10', function(assert) {
  function isBigEnough(element, index, array) {
    return element >= 10;
  }

  var list = new Dolphin.Collections.List([12, 5, 8, 130, 44]);
  assert.equal(list.every(isBigEnough), false);
});

QUnit.test('傳入 [12, 54, 18, 130, 44]，應判斷全部都 >= 10', function(assert) {
  function isBigEnough(element, index, array) {
    return element >= 10;
  }

  var list = new Dolphin.Collections.List([12, 54, 18, 130, 44]);
  assert.equal(list.every(isBigEnough), true);
});
