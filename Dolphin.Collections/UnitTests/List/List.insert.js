﻿QUnit.module('Dolphin.Collections.List.insert');

QUnit.test(
  '串列內容是[1,2,3]，在位置 -1 插入 4，應拋出例外 Dolphin.Exceptions.IndexOutOfRange',
  function(assert) {
    var list = new Dolphin.Collections.List([1, 2, 3]);
    assert.throws(
      function() {
        list.insert(-1, 4);
      },
      function(error) {
        return error instanceof Dolphin.Exceptions.IndexOutOfRange;
      }
    );
  }
);

QUnit.test(
  '串列內容是[1,2,3]，在位置 3 插入 4，應拋出例外 Dolphin.Exceptions.IndexOutOfRange',
  function(assert) {
    var list = new Dolphin.Collections.List([1, 2, 3]);
    assert.throws(
      function() {
        list.insert(3, 4);
      },
      function(error) {
        return error instanceof Dolphin.Exceptions.IndexOutOfRange;
      }
    );
  }
);

QUnit.test('串列內容是[]，在位置 0 插入 4，應拋出例外 Dolphin.Exceptions.IndexOutOfRange', function(
  assert
) {
  var list = new Dolphin.Collections.List();
  assert.throws(
    function() {
      list.insert(0, 4);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.IndexOutOfRange;
    }
  );
});

QUnit.test('串列內容是[1,2,3]，在位置 1 插入 4，串列內容應為 [1,4,2,3] 並回傳 List 本身', function(
  assert
) {
  var list = new Dolphin.Collections.List([1, 2, 3]);
  var result = list.insert(1, 4);
  assert.equal(list.count(), 4);
  assert.equal(list.get(0), 1);
  assert.equal(list.get(1), 4);
  assert.equal(list.get(2), 2);
  assert.equal(list.get(3), 3);
  assert.equal(list, result);
});
