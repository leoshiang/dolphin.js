﻿QUnit.module('Dolphin.Collections.List');

QUnit.test('確認成員有被定義', function(assert) {
  assert.equal(typeof Dolphin.Collections.List, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.add, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.append, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.assign, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.clear, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.clone, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.count, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.delete, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.difference, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.distinct, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.ensureFunction, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.ensureIndex, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.ensureList, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.equal, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.every, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.exchange, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.find, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.findIndex, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.filter, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.forEach, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.get, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.has, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.indexOf, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.indexOfItem, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.insert, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.intersection, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.map, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.move, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.put, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.remove, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.reverse, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.sort, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.subset, 'function');
  assert.equal(typeof Dolphin.Collections.List.prototype.union, 'function');
});

QUnit.test('呼叫建構子，應建立物件', function(assert) {
  var list = new Dolphin.Collections.List();
  assert.equal(typeof list, 'object');
});

QUnit.test('呼叫建構子傳入 List，應與傳入的 List 相同', function(assert) {
  var list1 = new Dolphin.Collections.List([1, 2, 3]);
  var list2 = new Dolphin.Collections.List(list1);
  assert.ok(list2.equal(list1));
});

QUnit.test('呼叫建構子傳入多個陣列，應加入每個陣列的元素', function(assert) {
  var list1 = new Dolphin.Collections.List([1, 2, 3]);
  var list2 = new Dolphin.Collections.List([4, 5, 6]);
  var list3 = new Dolphin.Collections.List([list1, list2]);
  assert.ok(list3.equal([1, 2, 3, 4, 5, 6]));
});

QUnit.test('呼叫建構子傳入多個項目，應將每個項目加入', function(assert) {
  var list = new Dolphin.Collections.List('a', 'b', 'c');
  assert.equal(list.count(), 3);
  assert.equal(list.get(0), 'a');
  assert.equal(list.get(1), 'b');
  assert.equal(list.get(2), 'c');
});

QUnit.test('呼叫建構子傳入多個陣列與項目，應將每個陣列與項目加入', function(assert) {
  var list = new Dolphin.Collections.List(['a', 'b', 'c'], 'g', ['d', 'e', 'f']);
  assert.equal(list.count(), 7);
  assert.equal(list.get(0), 'a');
  assert.equal(list.get(1), 'b');
  assert.equal(list.get(2), 'c');
  assert.equal(list.get(3), 'g');
  assert.equal(list.get(4), 'd');
  assert.equal(list.get(5), 'e');
  assert.equal(list.get(6), 'f');
});

QUnit.test('呼叫建構子傳入不同型別的項目，應將每個項目加入', function(assert) {
  var list = new Dolphin.Collections.List('a', 1, new Date(), {});
  assert.equal(list.count(), 4);
  assert.equal(list.get(0), 'a');
  assert.equal(list.get(1), 1);
  assert.ok(list.get(2) instanceof Date);
  assert.ok(list.get(3) instanceof Object);
});
