﻿QUnit.module('Dolphin.Collections.List.exchange');

QUnit.test('無項目，交換應拋出例外 Dolphin.Exceptions.IndexOutOfRange', function(assert) {
  var list = new Dolphin.Collections.List();
  assert.throws(
    function() {
      list.exchange(0, 1);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.IndexOutOfRange;
    }
  );
});

QUnit.test('三筆資料交換 -1 與 1 ，應拋出例外 Dolphin.Exceptions.IndexOutOfRange', function(
  assert
) {
  var list = new Dolphin.Collections.List();
  list.add(1);
  list.add(2);
  list.add(3);
  assert.throws(
    function() {
      list.exchange(-1, 1);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.IndexOutOfRange;
    }
  );
});

QUnit.test('三筆資料，交換 3 與 1 ，應拋出例外 Dolphin.Exceptions.IndexOutOfRange', function(
  assert
) {
  var list = new Dolphin.Collections.List();
  list.add(1);
  list.add(2);
  list.add(3);
  assert.throws(
    function() {
      list.exchange(3, 1);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.IndexOutOfRange;
    }
  );
});

QUnit.test('三筆資料交換 0 與 2 ，應交換成功並回傳 List 本身', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(1);
  list.add(2);
  list.add(3);
  var result = list.exchange(0, 2);
  assert.equal(list.get(0), 3);
  assert.equal(list.get(2), 1);
  assert.equal(result, list);
});
