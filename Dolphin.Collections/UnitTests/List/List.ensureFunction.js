﻿QUnit.module('Dolphin.Collections.List.ensureFunction');

QUnit.test('非 function 應拋出例外', function(assert) {
  var list = new Dolphin.Collections.List();
  assert.throws(
    function() {
      list.ensureFunction({});
    },
    function(error) {
      return error instanceof TypeError;
    }
  );
});

QUnit.test('傳入 function 應不拋出例外', function(assert) {
  var list = new Dolphin.Collections.List();
  list.ensureFunction(function() {});
  assert.ok(true);
});
