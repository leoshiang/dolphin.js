﻿QUnit.module('Dolphin.Collections.List.move');

QUnit.test('將串列 [1,2,3] 位置 2 項目移到 0 ，串列應改為 [3,1,2] 並回傳串列本身', function(
  assert
) {
  var list = new Dolphin.Collections.List([1, 2, 3]);
  var result = list.move(2, 0);
  assert.equal(list.count(), 3);
  assert.equal(list.get(0), 3);
  assert.equal(list.get(1), 1);
  assert.equal(list.get(2), 2);
  assert.equal(list, result);
});

QUnit.test('將串列 [1,2,3] 位置 1 項目移到 2 ，串列應改為 [1,3,2] 並回傳串列本身', function(
  assert
) {
  var list = new Dolphin.Collections.List([1, 2, 3]);
  var result = list.move(1, 2);
  assert.equal(list.count(), 3);
  assert.equal(list.get(0), 1);
  assert.equal(list.get(1), 3);
  assert.equal(list.get(2), 2);
  assert.equal(list, result);
});

QUnit.test('將串列 [1,2,3] 位置 -1 項目移到 -2 ，串列應改為 [1,3,2] 並回傳串列本身', function(
  assert
) {
  var list = new Dolphin.Collections.List([1, 2, 3]);
  var result = list.move(-1, -2);
  assert.equal(list.count(), 3);
  assert.equal(list.get(0), 1);
  assert.equal(list.get(1), 3);
  assert.equal(list.get(2), 2);
  assert.equal(list, result);
});
