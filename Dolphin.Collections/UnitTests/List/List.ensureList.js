﻿QUnit.module('Dolphin.Collections.List.ensureList');

QUnit.test('非 Dlphin.Collections.List 應拋出例外', function(assert) {
  var list = new Dolphin.Collections.List();
  assert.throws(
    function() {
      list.ensureList({});
    },
    function(error) {
      return error instanceof TypeError;
    }
  );
});

QUnit.test('傳入 Dlphin.Collections.List 應不拋出例外', function(assert) {
  var list = new Dolphin.Collections.List();
  list.ensureList(new Dolphin.Collections.List());
  assert.ok(true);
});
