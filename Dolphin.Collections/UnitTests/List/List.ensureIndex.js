﻿QUnit.module('Dolphin.Collections.List.ensureIndex');

QUnit.test('無項目，位置 0 應拋出例外', function(assert) {
  var list = new Dolphin.Collections.List();
  assert.throws(
    function() {
      list.ensureIndex(0);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.IndexOutOfRange;
    }
  );
});

QUnit.test('無項目，位置 -1 應拋出例外', function(assert) {
  var list = new Dolphin.Collections.List();
  assert.throws(
    function() {
      list.ensureIndex(-1);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.IndexOutOfRange;
    }
  );
});

QUnit.test('無項目，位置 1 應拋出例外', function(assert) {
  var list = new Dolphin.Collections.List();
  assert.throws(
    function() {
      list.ensureIndex(1);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.IndexOutOfRange;
    }
  );
});

QUnit.test('三筆資料，位置 -1 應拋出例外', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(1);
  list.add(2);
  list.add(3);
  assert.throws(
    function() {
      list.ensureIndex(-1);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.IndexOutOfRange;
    }
  );
});

QUnit.test('三筆資料，位置 3 應拋出例外', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(1);
  list.add(2);
  list.add(3);
  assert.throws(
    function() {
      list.ensureIndex(3);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.IndexOutOfRange;
    }
  );
});

QUnit.test('三筆資料，位置 1 應正確', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(1);
  list.add(2);
  list.add(3);
  list.ensureIndex(1);
  assert.ok(true);
});
