﻿QUnit.module('Dolphin.Collections.List.append');

QUnit.test('傳入 null，應拋出 Dolphin.Exceptions.NullException', function(assert) {
  assert.throws(
    function() {
      var list = new Dolphin.Collections.List();
      list.append(null);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.NullException;
    }
  );
});

QUnit.test('傳入非 List，應拋出 TypeError', function(assert) {
  assert.throws(
    function() {
      var list = new Dolphin.Collections.List();
      list.append([]);
    },
    function(error) {
      return error instanceof TypeError;
    }
  );
});

QUnit.test('傳入 list， list 的項目應被加入', function(assert) {
  var list1 = new Dolphin.Collections.List([1, 2, 3]);
  var list2 = new Dolphin.Collections.List().append(list1);
  assert.expect(3);
  list1.forEach(function(item) {
    assert.ok(list2.has(item));
  });
});
