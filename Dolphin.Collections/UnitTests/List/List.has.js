﻿QUnit.module('Dolphin.Collections.List.has');

QUnit.test('無項目，應回傳 false', function(assert) {
  var list = new Dolphin.Collections.List();
  assert.equal(list.has(1), false);
});

QUnit.test('加入 [1] 並判斷是否擁有 [1]，應回傳 true', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(1);
  assert.equal(list.has(1), true);
});

QUnit.test('判斷 [1] 是否擁有 2 ，應回傳 false', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(1);
  assert.equal(list.has(2), false);
});

QUnit.test('判斷 [a,b,b] 是否擁有 b ，應回傳 true', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add('a');
  list.add('b');
  list.add('b');
  assert.equal(list.has('b'), 1);
});
