﻿QUnit.module('Dolphin.Collections.List.forEach');

QUnit.test('callback 傳入 null，應拋出 TypeError', function(assert) {
  assert.throws(
    function() {
      var list = new Dolphin.Collections.List();
      list.forEach(null);
    },
    function(error) {
      return error instanceof TypeError;
    }
  );
});

QUnit.test('callback 傳入 object，應拋出 TypeError', function(assert) {
  assert.throws(
    function() {
      var list = new Dolphin.Collections.List();
      list.forEach({});
    },
    function(error) {
      return error instanceof TypeError;
    }
  );
});

QUnit.test('三筆資料，callback 應呼叫三次', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(1);
  list.add(2);
  list.add(3);
  assert.expect(3);
  list.forEach(function(item, index) {
    assert.equal(item, index + 1);
  });
});
