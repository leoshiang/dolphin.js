﻿QUnit.module('Dolphin.Collections.List.get');

QUnit.test('無項目取位置 1 ，應拋出例外 Dolphin.Exceptions.IndexOutOfRange', function(assert) {
  var list = new Dolphin.Collections.List();
  assert.throws(
    function() {
      list.get(1);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.IndexOutOfRange;
    }
  );
});

QUnit.test('無項目取位置 -1 ，應拋出例外 Dolphin.Exceptions.IndexOutOfRange', function(assert) {
  var list = new Dolphin.Collections.List();
  assert.throws(
    function() {
      list.get(-1);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.IndexOutOfRange;
    }
  );
});

QUnit.test('三筆資料取第四個，應拋出例外', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(1);
  list.add(2);
  list.add(3);
  assert.throws(
    function() {
      list.get(3);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.IndexOutOfRange;
    }
  );
});

QUnit.test('三筆資料，取每一個應相同', function(assert) {
  var list = new Dolphin.Collections.List();
  list.add(1);
  list.add(2);
  list.add(3);
  assert.equal(list.get(0), 1);
  assert.equal(list.get(1), 2);
  assert.equal(list.get(2), 3);
});
