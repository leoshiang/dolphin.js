﻿QUnit.module('Dolphin.Collections.List.findIndex');
QUnit.test('callback 傳入 null，應拋出 TypeError', function(assert) {
  assert.throws(
    function() {
      var list = new Dolphin.Collections.List();
      list.findIndex(null);
    },
    function(error) {
      return error instanceof TypeError;
    }
  );
});

QUnit.test('callback 傳入 object，應拋出 TypeError', function(assert) {
  assert.throws(
    function() {
      var list = new Dolphin.Collections.List();
      list.findIndex({});
    },
    function(error) {
      return error instanceof TypeError;
    }
  );
});

QUnit.test('無項目，應回傳 -1', function(assert) {
  function isStringA(item) {
    return item === 'a';
  }

  var list = new Dolphin.Collections.List();
  assert.equal(list.findIndex(isStringA), -1);
});

QUnit.test('有資料，應回傳該物件索引值', function(assert) {
  function isStringC(item) {
    return item === 'c';
  }

  var list = new Dolphin.Collections.List();
  list.add('a');
  list.add('b');
  list.add('c');
  assert.equal(list.findIndex(isStringC), 2);
});
