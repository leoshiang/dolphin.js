﻿QUnit.module('Dolphin.Collections.Queue.dequeue');

QUnit.test('無項目，應拋出例外 Dolphin.Exceptions.IndexOutOfRange', function(assert) {
  var queue = new Dolphin.Collections.Queue();
  assert.throws(
    function() {
      queue.dequeue();
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.IndexOutOfRange;
    }
  );
});

QUnit.test('加入 1 個項目再取出，數量應等於 0', function(assert) {
  var queue = new Dolphin.Collections.Queue();
  queue.enqueue(1);
  queue.dequeue();
  assert.equal(queue.count(), 0);
});
