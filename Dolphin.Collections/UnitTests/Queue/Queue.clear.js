﻿QUnit.module('Dolphin.Collections.Queue.clear');

QUnit.test('無項目，數量應回傳 0', function(assert) {
  var queue = new Dolphin.Collections.Queue();
  queue.clear();
  assert.equal(queue.count(), 0);
});

QUnit.test('加入 1 個項目，數量應回傳 0', function(assert) {
  var queue = new Dolphin.Collections.Queue();
  queue.enqueue(1);
  queue.clear();
  assert.equal(queue.count(), 0);
});
