﻿QUnit.module('Dolphin.Collections.Queue.isEmpty');

QUnit.test('無項目，應回傳 true', function(assert) {
  var queue = new Dolphin.Collections.Queue();
  assert.ok(queue.isEmpty());
});

QUnit.test('加入 1 個項目，應回傳 false', function(assert) {
  var queue = new Dolphin.Collections.Queue();
  queue.enqueue(1);
  assert.notOk(queue.isEmpty());
});
