﻿QUnit.module('Dolphin.Collections.Queue');

QUnit.test('確認成員有被定義', function(assert) {
  assert.equal(typeof Dolphin.Collections.Queue, 'function');
  assert.equal(typeof Dolphin.Collections.Queue.prototype.clear, 'function');
  assert.equal(typeof Dolphin.Collections.Queue.prototype.count, 'function');
  assert.equal(typeof Dolphin.Collections.Queue.prototype.isEmpty, 'function');
  assert.equal(typeof Dolphin.Collections.Queue.prototype.enqueue, 'function');
  assert.equal(typeof Dolphin.Collections.Queue.prototype.dequeue, 'function');
  assert.equal(typeof Dolphin.Collections.Queue.prototype.front, 'function');
});

QUnit.test('呼叫建構子，應建立物件', function(assert) {
  var queue = new Dolphin.Collections.Queue();
  assert.equal(typeof queue, 'object');
});

QUnit.test('呼叫建構子傳入多個引數，應將每個引數加入', function(assert) {
  var queue = new Dolphin.Collections.Queue(1, 2, 3);
  assert.equal(queue.count(), 3);
});

QUnit.test('呼叫建構子傳入多個陣列引數，應將陣列的每個項目加入', function(assert) {
  var queue = new Dolphin.Collections.Queue([1, 2, 3, 4], [5], [6, 7]);
  assert.equal(queue.count(), 7);
});
