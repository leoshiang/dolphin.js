﻿QUnit.module('Dolphin.Collections.Queue.front');

QUnit.test('加入 1 個項目，應等於加入的項目', function(assert) {
  var queue = new Dolphin.Collections.Queue();
  queue.enqueue(1);
  assert.equal(queue.front(), 1);
});
