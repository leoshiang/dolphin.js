﻿QUnit.module('Dolphin.Collections.Queue.enqueue');

QUnit.test('加入 1 個項目，數量應等於 1', function(assert) {
  var queue = new Dolphin.Collections.Queue();
  queue.enqueue(1);
  assert.equal(queue.count(), 1);
});
