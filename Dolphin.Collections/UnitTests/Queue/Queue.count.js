﻿QUnit.module('Dolphin.Collections.Queue.count');

QUnit.test('無項目，應回傳 0', function(assert) {
  var queue = new Dolphin.Collections.Queue();
  assert.equal(queue.count(), 0);
});

QUnit.test('加入 1 個項目，應回傳 1', function(assert) {
  var queue = new Dolphin.Collections.Queue();
  queue.enqueue(1);
  assert.equal(queue.count(), 1);
});
