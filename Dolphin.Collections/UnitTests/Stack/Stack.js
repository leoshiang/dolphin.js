﻿QUnit.module('Dolphin.Collections.Stack');

QUnit.test('確認成員有被定義', function(assert) {
  assert.equal(typeof Dolphin.Collections.Stack, 'function');
  assert.equal(typeof Dolphin.Collections.Stack.prototype.clear, 'function');
  assert.equal(typeof Dolphin.Collections.Stack.prototype.count, 'function');
  assert.equal(typeof Dolphin.Collections.Stack.prototype.isEmpty, 'function');
  assert.equal(typeof Dolphin.Collections.Stack.prototype.push, 'function');
  assert.equal(typeof Dolphin.Collections.Stack.prototype.pop, 'function');
});

QUnit.test('呼叫建構子，應建立物件', function(assert) {
  var stack = new Dolphin.Collections.Stack();
  assert.equal(typeof stack, 'object');
});
