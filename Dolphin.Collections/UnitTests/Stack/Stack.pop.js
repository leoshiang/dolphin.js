﻿QUnit.module('Dolphin.Collections.Stack.pop');

QUnit.test('無項目，應拋出例外 Dolphin.Exceptions.IndexOutOfRange', function(assert) {
  var stack = new Dolphin.Collections.Stack();
  assert.throws(
    function() {
      stack.pop();
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.IndexOutOfRange;
    }
  );
});

QUnit.test('推入 1 個資料，取出的資料應該相同', function(assert) {
  var stack = new Dolphin.Collections.Stack();
  stack.push(1234);
  assert.equal(stack.pop(), 1234);
});

QUnit.test('推入 1 個資料，數量應為 1', function(assert) {
  var stack = new Dolphin.Collections.Stack();
  stack.push(1234);
  assert.equal(stack.count(), 1);
});
