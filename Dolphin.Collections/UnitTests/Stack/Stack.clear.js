﻿QUnit.module('Dolphin.Collections.Stack.clear');

QUnit.test('無項目，數量應回傳 0', function(assert) {
  var stack = new Dolphin.Collections.Stack();
  stack.clear();
  assert.equal(stack.count(), 0);
});

QUnit.test('加入 1 個項目，數量應回傳 0', function(assert) {
  var stack = new Dolphin.Collections.Stack();
  stack.push(1);
  stack.clear();
  assert.equal(stack.count(), 0);
});
