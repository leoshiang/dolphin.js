﻿QUnit.module('Dolphin.Collections.Stack.push');

QUnit.test('無參數，應加入 1 個 undefined 項目', function(assert) {
  var stack = new Dolphin.Collections.Stack();
  stack.push();
  assert.equal(stack.peek(), undefined);
});

QUnit.test('推入 1 個項目，數量應為 1', function(assert) {
  var stack = new Dolphin.Collections.Stack();
  stack.push(1);
  assert.equal(stack.count(), 1);
});

QUnit.test('使用方法鏈推入 3 個項目，數量應為 3', function(assert) {
  var stack = new Dolphin.Collections.Stack();
  stack
    .push(1)
    .push(2)
    .push(3);
  assert.equal(stack.count(), 3);
});
