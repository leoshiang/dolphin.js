﻿QUnit.module('Dolphin.Collections.Stack.count');

QUnit.test('無項目，應回傳 0', function(assert) {
  var stack = new Dolphin.Collections.Stack();
  assert.equal(stack.count(), 0);
});

QUnit.test('加入 1 個項目，應回傳 1', function(assert) {
  var stack = new Dolphin.Collections.Stack();
  stack.push(1);
  assert.equal(stack.count(), 1);
});
