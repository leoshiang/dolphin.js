﻿QUnit.module('Dolphin.Collections.Stack.isEmpty');

QUnit.test('無項目，應回傳 true', function(assert) {
  var stack = new Dolphin.Collections.Stack();
  assert.ok(stack.isEmpty());
});

QUnit.test('加入 1 個項目，應回傳 false', function(assert) {
  var stack = new Dolphin.Collections.Stack();
  stack.push(1);
  assert.notOk(stack.isEmpty());
});
