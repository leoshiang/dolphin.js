Dolphin.namespace('Collections').Queue = function() {
  this.items = [];
  this.initFromArguments.apply(this, arguments);
};

Dolphin.Collections.CollectionMixin.call(Dolphin.Collections.Queue.prototype);
Dolphin.Collections.QueueMixin.call(Dolphin.Collections.Queue.prototype);
