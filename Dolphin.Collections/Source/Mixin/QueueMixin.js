Dolphin.namespace('Collections').QueueMixin = function() {
  this.initFromArguments = function() {
    for (var i = 0; i < arguments.length; i++) {
      var item = arguments[i];
      if (item instanceof Array) {
        this.items = this.items.concat(item);
      } else {
        this.items.push(item);
      }
    }
  };

  this.enqueue = function(element) {
    this.items.push(element);
    return this;
  };

  this.dequeue = function() {
    if (this.isEmpty()) {
      throw new Dolphin.Exceptions.IndexOutOfRange();
    }
    return this.items.shift();
  };

  this.front = function() {
    if (this.isEmpty()) {
      throw new Dolphin.Exceptions.IndexOutOfRange();
    }
    return this.items[0];
  };

  return this;
};
