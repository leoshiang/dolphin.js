Dolphin.namespace('Collections').CollectionMixin = function() {
  this.clear = function() {
    this.items = [];
    return this;
  };

  this.count = function() {
    return this.items.length;
  };

  this.isEmpty = function() {
    return this.items.length === 0;
  };

  return this;
};
