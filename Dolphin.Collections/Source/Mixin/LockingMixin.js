Dolphin.namespace('Collections').LockingMixin = function() {
  this.updating = false;

  this.beginUpdate = function() {
    this.updating = true;
  };

  this.isUpdating = function() {
    return this.updating;
  };

  this.onUpdated = function() {};

  this.endUpdate = function() {
    this.updating = false;
    this.onUpdated();
  };

  return this;
};
