Dolphin.namespace('Collections').Stack = function() {
  this.items = [];
};

Dolphin.Collections.Stack.prototype = {
  push: function(item) {
    this.items.push(item);

    return this;
  },

  pop: function(item) {
    if (this.isEmpty()) {
      throw new Dolphin.Exceptions.IndexOutOfRange();
    }
    return this.items.pop();
  },

  peek: function() {
    if (this.isEmpty()) {
      throw new Dolphin.Exceptions.IndexOutOfRange();
    }
    return this.items[this.items.length - 1];
  }
};

Dolphin.Collections.CollectionMixin.call(Dolphin.Collections.Stack.prototype);
