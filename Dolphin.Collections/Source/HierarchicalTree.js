Dolphin.namespace('Collections').HierarchicalTreeNode = function(id) {
  this.id = id;
  this.children = new Dolphin.Collections.List();
  this.data = null;
};

Dolphin.Collections.HierarchicalTreeNode.prototype = {
  addChild: function(child) {
    this.children.add(child);
  },

  findChild: function(callback) {
    return this.children.find(callback);
  },

  findChildById: function(id) {
    return this.findChild(function(child) {
      return child.id === id;
    });
  },

  count: function() {
    return this.children.count();
  }
};

Dolphin.namespace('Collections').HierarchicalTree = function(rootId) {
  this.root = new Dolphin.Collections.HierarchicalTreeNode(rootId);
};

Dolphin.namespace('Collections').HierarchicalTree.prototype = {
  count: function() {
    function getChildrenCount(node) {
      var result = 0;
      node.children.forEach(function(child) {
        result += getChildrenCount(child);
      });
      return result + node.count();
    }

    return getChildrenCount(this.root) + 1;
  },

  fromList: function(list, options) {
    if (!(list instanceof Dolphin.Collections.List)) {
      throw new TypeError();
    }

    function cachedNode(cachedNodes, id) {
      var result = cachedNodes[id];
      if (result === undefined) {
        result = new Dolphin.Collections.HierarchicalTreeNode(id);
        cachedNodes[id] = result;
      }
      return result;
    }

    options = options || {};
    var idKey = options.idKey || 'id';
    var parentKey = options.parentKey || 'parentId';
    var rootId = options.rootId || '00000000-0000-0000-0000-000000000000';
    var cachedNodes = [];

    this.root = cachedNode(cachedNodes, rootId);
    list.forEach(function(item) {
      var childId = item[idKey];
      if (cachedNodes[childId] !== undefined) {
        throw 'Duplicated id ' + childId;
      }

      // 建立 child node
      var childNode = new Dolphin.Collections.HierarchicalTreeNode(childId);

      // 複製 item 的屬性到節點的 data
      childNode.data = Object.assign({}, item);

      // 將 child node 放入 cache
      cachedNodes[childId] = childNode;

      // 將 child node 加到 parent 之下
      var parentNode = cachedNode(cachedNodes, item[parentKey]);
      parentNode.addChild(childNode);
    });
  },

  toList: function(nodeToListCallback) {
    function convertChildrenToList(node, callback, list) {
      node.children.forEach(function(child) {
        list.add(callback(child));
      });

      node.children.forEach(function(child) {
        convertChildrenToList(child, callback, list);
      });
    }

    var result = new Dolphin.Collections.List();
    convertChildrenToList(root, nodeToListCallback, result);
    return result;
  }
};
