Dolphin.namespace('Collections').List = function() {
  this.items = [];

  var _self = this;

  function addItem(item) {
    if (item instanceof Dolphin.Collections.List) {
      _self.append(item);
    } else {
      _self.items.push(item);
    }
  }

  for (var i = 0; i < arguments.length; i++) {
    var source = arguments[i];
    if (source instanceof Array) {
      source.forEach(
        function(item) {
          addItem(item);
        }.bind(this)
      );
    } else {
      addItem(source);
    }
  }
};

Dolphin.Collections.CollectionMixin.call(Dolphin.Collections.List.prototype);
Dolphin.Collections.ListMixin.call(Dolphin.Collections.List.prototype);
