Dolphin.namespace('Collections').QueueElement = function(element, priority) {
  this.element = element;
  this.priority = priority;
};

Dolphin.namespace('Collections').PriorityQueue = function() {
  this.items = [];
  this.initFromArguments.apply(this, arguments);
};

Dolphin.Collections.CollectionMixin.call(Dolphin.Collections.PriorityQueue.prototype);
Dolphin.Collections.QueueMixin.call(Dolphin.Collections.PriorityQueue.prototype);

Dolphin.Collections.PriorityQueue.prototype.enqueue = function(element, priority) {
  var queueElement = new Dolphin.Collections.QueueElement(element, priority);
  if (this.isEmpty()) {
    this.items.push(queueElement);
  } else {
    var added = false;
    for (var i = 0; i < this.items.length; i++) {
      if (queueElement.priority < this.items[i].priority) {
        this.items.splice(i, 0, queueElement);
        added = true;
        break;
      }
    }
    if (!added) {
      this.items.push(queueElement);
    }
  }
  return this;
};
