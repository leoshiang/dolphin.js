﻿QUnit.module('Dolphin.getType');

QUnit.test('字串', function(assert) {
  assert.equal(Dolphin.getType('abc'), Dolphin.Types.String);
});

QUnit.test('空字串', function(assert) {
  assert.equal(Dolphin.getType('abc'), Dolphin.Types.String);
});

QUnit.test('Boolean False', function(assert) {
  assert.equal(Dolphin.getType(true), Dolphin.Types.Boolean);
});

QUnit.test('Boolean True', function(assert) {
  assert.equal(Dolphin.getType(false), Dolphin.Types.Boolean);
});

QUnit.test('Number 整數 1', function(assert) {
  assert.equal(Dolphin.getType(1), Dolphin.Types.Number);
});

QUnit.test('Number 整數 0', function(assert) {
  assert.equal(Dolphin.getType(0), Dolphin.Types.Number);
});

QUnit.test('Number 浮點數 1.0', function(assert) {
  assert.equal(Dolphin.getType(1.0), Dolphin.Types.Number);
});

QUnit.test('Number 浮點數 0.0', function(assert) {
  assert.equal(Dolphin.getType(0.0), Dolphin.Types.Number);
});

QUnit.test('Null', function(assert) {
  assert.equal(Dolphin.getType(null), Dolphin.Types.Null);
});

QUnit.test('Undefined', function(assert) {
  assert.equal(Dolphin.getType(undefined), Dolphin.Types.Undefined);
});

QUnit.test('無參數', function(assert) {
  assert.equal(Dolphin.getType(), Dolphin.Types.Undefined);
});

QUnit.test('Object', function(assert) {
  assert.equal(Dolphin.getType({}), Dolphin.Types.Object);
});

QUnit.test('Function', function(assert) {
  assert.equal(Dolphin.getType(function() {}), Dolphin.Types.Function);
});

QUnit.test('Array', function(assert) {
  assert.equal(Dolphin.getType([]), Dolphin.Types.Array);
});
