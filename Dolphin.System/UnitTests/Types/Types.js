﻿QUnit.module('Dolphin.Types');

QUnit.test('確認成員有被定義', function(assert) {
  assert.equal(typeof Dolphin.Types.Array, 'string');
  assert.equal(typeof Dolphin.Types.Boolean, 'string');
  assert.equal(typeof Dolphin.Types.Function, 'string');
  assert.equal(typeof Dolphin.Types.Null, 'string');
  assert.equal(typeof Dolphin.Types.Number, 'string');
  assert.equal(typeof Dolphin.Types.Object, 'string');
  assert.equal(typeof Dolphin.Types.String, 'string');
  assert.equal(typeof Dolphin.Types.Undefined, 'string');
});
