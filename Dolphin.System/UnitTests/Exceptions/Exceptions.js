﻿QUnit.module('Dolphin.Exceptions');

QUnit.test('例外是否有被定義', function(assert) {
  assert.equal(typeof Dolphin.Exceptions.AssertionError, 'function');
  assert.equal(typeof Dolphin.Exceptions.IndexOutOfRange, 'function');
  assert.equal(typeof Dolphin.Exceptions.InvalidArgument, 'function');
  assert.equal(typeof Dolphin.Exceptions.InvalidNamespace, 'function');
  assert.equal(typeof Dolphin.Exceptions.NonExistentElement, 'function');
});

QUnit.test('拋出 AssertionError', function(assert) {
  assert.throws(
    function() {
      throw new Dolphin.Exceptions.AssertionError();
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.AssertionError;
    }
  );
});

QUnit.test('拋出 InvalidArgument', function(assert) {
  assert.throws(
    function() {
      throw new Dolphin.Exceptions.InvalidArgument();
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.InvalidArgument;
    }
  );
});

QUnit.test('拋出 NonExistentElement', function(assert) {
  assert.throws(
    function() {
      throw new Dolphin.Exceptions.NonExistentElement();
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.NonExistentElement;
    }
  );
});

QUnit.test('拋出 InvalidNamespace', function(assert) {
  assert.throws(
    function() {
      throw new Dolphin.Exceptions.InvalidNamespace();
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.InvalidNamespace;
    }
  );
});

QUnit.test('拋出 IndexOutOfRange', function(assert) {
  assert.throws(
    function() {
      throw new Dolphin.Exceptions.IndexOutOfRange();
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.IndexOutOfRange;
    }
  );
});
