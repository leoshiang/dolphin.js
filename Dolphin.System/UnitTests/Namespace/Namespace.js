﻿QUnit.module('Dolphin.Namespace');

QUnit.test('測試成員是否有被定義', function(assert) {
  assert.equal(typeof Namespace, 'function');
  assert.equal(typeof Namespace.prototype.traverse, 'function');
  assert.equal(typeof Namespace.prototype.namespace, 'function');
  assert.equal(typeof Namespace.prototype.reflection, 'function');
});
