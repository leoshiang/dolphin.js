﻿QUnit.module('Namespace.reflection');

QUnit.test('函式 reflection 有被定義', function(assert) {
  var testNamespace = new Namespace();
  assert.equal(typeof testNamespace.reflection, 'function');
});

QUnit.test('定義函式，透過反射取得並呼叫', function(assert) {
  var testNamespace = new Namespace();
  testNamespace.namespace('System.Math').Add = function(a, b) {
    return a + b;
  };
  var add = testNamespace.reflection('System.Math.Add');
  var result = add(1, 2);
  assert.equal(result, 3);
});

QUnit.test('函式定義在命名空間根節點，透過反射取得並呼叫', function(assert) {
  var testNamespace = new Namespace();
  testNamespace.namespace('').Add = function(a, b) {
    return a + b;
  };
  var result = testNamespace.reflection('Add')(1, 2);
  assert.equal(result, 3);
});

QUnit.test('中文-定義函式，透過反射取得並呼叫', function(assert) {
  var testNamespace = new Namespace();
  testNamespace.namespace('海豚.數學').相加 = function(a, b) {
    return a + b;
  };
  var result = testNamespace.reflection('海豚.數學.相加')(1, 2);
  assert.equal(result, 3);
});

QUnit.test('中文-函式定義在命名空間根節點，透過反射取得並呼叫', function(assert) {
  var testNamespace = new Namespace();
  testNamespace.namespace('').相加 = function(a, b) {
    return a + b;
  };
  var result = testNamespace.reflection('相加')(1, 2);
  assert.equal(result, 3);
});
