﻿QUnit.module('Dolphin.getObjectSize');

QUnit.test("'abc'，應回傳 3", function(assert) {
  assert.equal(Dolphin.getObjectSize('abc'), 5);
});

QUnit.test('空字串，應回傳 2', function(assert) {
  assert.equal(Dolphin.getObjectSize(''), 2);
});

QUnit.test('true，應回傳 4', function(assert) {
  assert.equal(Dolphin.getObjectSize(true), 4);
});

QUnit.test('false，應回傳 5', function(assert) {
  assert.equal(Dolphin.getObjectSize(false), 5);
});

QUnit.test('整數 1，應回傳 1', function(assert) {
  assert.equal(Dolphin.getObjectSize(1), 1);
});

QUnit.test('浮點數 1.0，應回傳 1', function(assert) {
  assert.equal(Dolphin.getObjectSize(1.0), 1);
});

QUnit.test('浮點數 1.1，應回傳 3', function(assert) {
  assert.equal(Dolphin.getObjectSize(1.1), 3);
});

QUnit.test('Null，應回傳 4', function(assert) {
  assert.equal(Dolphin.getObjectSize(null), 4);
});

QUnit.test('Undefined，應拋出 TypeError', function(assert) {
  assert.throws(
    function() {
      assert.equal(Dolphin.getObjectSize(undefined), 0);
    },
    function(error) {
      return error instanceof TypeError;
    }
  );
});

QUnit.test('無參數，應拋出 TypeError', function(assert) {
  assert.throws(
    function() {
      assert.equal(Dolphin.getObjectSize(), 0);
    },
    function(error) {
      return error instanceof TypeError;
    }
  );
});

QUnit.test('空物件，應回傳 2', function(assert) {
  assert.equal(Dolphin.getObjectSize({}), 2);
});

QUnit.test('Function，應拋出 TypeError', function(assert) {
  assert.throws(
    function() {
      assert.equal(Dolphin.getObjectSize(function() {}), 0);
    },
    function(error) {
      return error instanceof TypeError;
    }
  );
});

QUnit.test('[1,2,3]，應回傳 7', function(assert) {
  assert.equal(Dolphin.getObjectSize([1, 2, 3]), 7);
});
