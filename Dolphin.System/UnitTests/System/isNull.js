﻿QUnit.module('Dolphin.isNull');

QUnit.test('無參數，應回傳 true', function(assert) {
  assert.equal(Dolphin.isNull(), true);
});

QUnit.test('null，應回傳 true', function(assert) {
  assert.equal(Dolphin.isNull(null), true);
});

QUnit.test('undefined，應回傳 true', function(assert) {
  assert.equal(Dolphin.isNull(undefined), true);
});

QUnit.test('true，應回傳 false', function(assert) {
  assert.equal(Dolphin.isNull(true), false);
});

QUnit.test('false，應回傳 false', function(assert) {
  assert.equal(Dolphin.isNull(false), false);
});

QUnit.test('空陣列，應回傳 false', function(assert) {
  var isNull = Dolphin.isNull;
  assert.equal(Dolphin.isNull([]), false);
});

QUnit.test('非空陣列，應回傳 false', function(assert) {
  assert.equal(Dolphin.isNull([1]), false);
});

QUnit.test('空字串，應回傳 false', function(assert) {
  assert.equal(Dolphin.isNull(''), false);
});

QUnit.test('非空字串，應回傳 false', function(assert) {
  assert.equal(Dolphin.isNull('xyz'), false);
});

QUnit.test('沒有 Key 的物件，應回傳 false', function(assert) {
  assert.equal(Dolphin.isNull({}), false);
});

QUnit.test('有 Key 的物件，應回傳 false', function(assert) {
  assert.equal(Dolphin.isNull({ name: 'test' }), false);
});

QUnit.test('數字 0，應回傳 false', function(assert) {
  assert.equal(Dolphin.isNull(0), false);
});
