﻿QUnit.module('Dolphin.getArgumentNames');

QUnit.test('無參數的函數，應回傳 0', function(assert) {
  var args = Dolphin.getArgumentNames(function() {});
  assert.equal(args.length, 0);
});

QUnit.test('三個參數的函數，應回傳 3', function(assert) {
  var args = Dolphin.getArgumentNames(function(a, b, c) {});
  assert.equal(args.length, 3);
  assert.equal(args[0], 'a');
  assert.equal(args[1], 'b');
  assert.equal(args[2], 'c');
});
