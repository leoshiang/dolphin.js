﻿QUnit.module('Dolphin');

QUnit.test('確認成員有被定義', function(assert) {
  assert.equal(typeof Dolphin.getType, 'function');
  assert.equal(typeof Dolphin.isEmpty, 'function');
  assert.equal(typeof Dolphin.isNull, 'function');
  assert.equal(typeof Dolphin.asString, 'function');
  assert.equal(typeof Dolphin.getArgumentNames, 'function');
  assert.equal(typeof Dolphin.getObjectSize, 'function');
  assert.equal(typeof Dolphin.deepEqual, 'function');
});
