﻿QUnit.module('Dolphin.isEmpty');

QUnit.test('無參數，應回傳 true', function(assert) {
  assert.equal(Dolphin.isEmpty(), true);
});

QUnit.test('null，應回傳 true', function(assert) {
  assert.equal(Dolphin.isEmpty(null), true);
});

QUnit.test('undefined，應回傳 true', function(assert) {
  assert.equal(Dolphin.isEmpty(undefined), true);
});

QUnit.test('true，應回傳 true', function(assert) {
  assert.equal(Dolphin.isEmpty(true), false);
});

QUnit.test('false，應回傳 true', function(assert) {
  assert.equal(Dolphin.isEmpty(false), true);
});

QUnit.test('空陣列，應回傳 true', function(assert) {
  var isEmpty = Dolphin.isEmpty;
  assert.equal(Dolphin.isEmpty([]), true);
});

QUnit.test('非空陣列，應回傳 false', function(assert) {
  assert.equal(Dolphin.isEmpty([1]), false);
});

QUnit.test('空字串，應回傳 true', function(assert) {
  assert.equal(Dolphin.isEmpty(''), true);
});

QUnit.test('非空字串，應回傳 false', function(assert) {
  assert.equal(Dolphin.isEmpty('xyz'), false);
});

QUnit.test('沒有 Key 的物件，應回傳 true', function(assert) {
  assert.equal(Dolphin.isEmpty({}), true);
});

QUnit.test('有 Key 的物件，應回傳 false', function(assert) {
  assert.equal(Dolphin.isEmpty({ name: 'test' }), false);
});

QUnit.test('數字 0，應回傳 true', function(assert) {
  assert.equal(Dolphin.isEmpty(0), true);
});

QUnit.test('數字 1，應回傳 false', function(assert) {
  assert.equal(Dolphin.isEmpty(1), false);
});
