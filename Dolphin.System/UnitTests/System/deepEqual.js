﻿QUnit.module('Dolphin.deepEqual');

QUnit.test('無參數，應回傳 true', function(assert) {
  assert.ok(Dolphin.deepEqual());
});

QUnit.test('空物件，應回傳 true', function(assert) {
  assert.ok(Dolphin.deepEqual({}, {}));
});

QUnit.test('有一個物件是空物件，應回傳 false', function(assert) {
  assert.notOk(Dolphin.deepEqual({ name: 'a' }, {}));
});

QUnit.test('有一個物件屬性較多，應回傳 false', function(assert) {
  var v1 = { name: 'a' };
  var v2 = { name: 'a', age: 10 };
  assert.notOk(Dolphin.deepEqual(v1, v2));
});

QUnit.test('兩個多屬性，應回傳 true', function(assert) {
  var v1 = { name: 'a', age: 10 };
  var v2 = { name: 'a', age: 10 };
  assert.ok(Dolphin.deepEqual(v1, v2));
});
