﻿QUnit.module('Dolphin.asString');

QUnit.test('無參數，應回傳 "undefined"', function(assert) {
  assert.equal(Dolphin.asString(), Dolphin.Types.Undefined);
});

QUnit.test('null，應回傳 "null"', function(assert) {
  assert.equal(Dolphin.asString(null), 'null');
});

QUnit.test('undefined，應回傳 "undefined"', function(assert) {
  assert.equal(Dolphin.asString(undefined), Dolphin.Types.Undefined);
});

QUnit.test('true，應回傳 "boolean:true"', function(assert) {
  assert.equal(Dolphin.asString(true), 'boolean:true');
});

QUnit.test('false，應回傳 "boolean:false"', function(assert) {
  assert.equal(Dolphin.asString(false), 'boolean:false');
});

QUnit.test('[]，應回傳 "array[0]"', function(assert) {
  var asString = Dolphin.asString;
  assert.equal(Dolphin.asString([]), 'array[0]');
});

QUnit.test('[1]，應回傳 "array[1]"', function(assert) {
  assert.equal(Dolphin.asString([1]), 'array[1]');
});

QUnit.test("''，應回傳 ''", function(assert) {
  assert.equal(Dolphin.asString(''), "string:''");
});

QUnit.test("'xyz'，應回傳 'xyz'", function(assert) {
  assert.equal(Dolphin.asString('xyz'), "string:'xyz'");
});

QUnit.test("{}，應回傳 '{}'", function(assert) {
  assert.equal(Dolphin.asString({}), 'object:{}');
});

QUnit.test('{name: "test"}，應回傳 object:{"name":"test"}', function(assert) {
  assert.equal(Dolphin.asString({ name: 'test' }), 'object:{"name":"test"}');
});

QUnit.test('0，應回傳 number:0', function(assert) {
  assert.equal(Dolphin.asString(0), 'number:0');
});
