Dolphin.namespace('').Types = {
  Array: 'array',
  Boolean: 'boolean',
  Function: 'function',
  Null: 'null',
  Number: 'number',
  Object: 'object',
  String: 'string',
  Undefined: 'undefined'
};

Dolphin.namespace('').getType = function(object) {
  var result;
  if (object === null) {
    result = Dolphin.Types.Null;
  } else if (object instanceof Array) {
    result = Dolphin.Types.Array;
  } else {
    result = typeof object;
  }
  return result;
};
