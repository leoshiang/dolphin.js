Dolphin.namespace('Exceptions').NonExistentElement = function(message) {
  this.message = message || '項目不存在';
  this.name = 'NonExistentElement';
  this.stack = new Error().stack;
};
Dolphin.Exceptions.NonExistentElement.prototype = Object.create(Error.prototype);
Dolphin.Exceptions.NonExistentElement.prototype.constructor = Dolphin.Exceptions.NonExistentElement;
