Dolphin.namespace('Exceptions').InvalidArgument = function(message) {
  this.message = message || '無效的參數';
  this.name = 'InvalidArgument';
  this.stack = new Error().stack;
};
Dolphin.Exceptions.InvalidArgument.prototype = Object.create(Error.prototype);
Dolphin.Exceptions.InvalidArgument.prototype.constructor = Dolphin.Exceptions.InvalidArgument;
