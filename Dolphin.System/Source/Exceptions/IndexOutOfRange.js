Dolphin.namespace('Exceptions').IndexOutOfRange = function(message) {
  this.message = message || '索引值超出範圍';
  this.name = 'IndexOutOfRange';
  this.stack = new Error().stack;
};
Dolphin.Exceptions.IndexOutOfRange.prototype = Object.create(Error.prototype);
Dolphin.Exceptions.IndexOutOfRange.prototype.constructor = Dolphin.Exceptions.IndexOutOfRange;
