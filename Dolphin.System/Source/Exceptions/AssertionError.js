Dolphin.namespace('Exceptions').AssertionError = function(message) {
  this.message = message || 'Assert exception.';
  this.name = 'AssertionError';
  this.stack = new Error().stack;
};
Dolphin.Exceptions.AssertionError.prototype = Object.create(Error.prototype);
Dolphin.Exceptions.AssertionError.prototype.constructor = Dolphin.Exceptions.AssertionError;
