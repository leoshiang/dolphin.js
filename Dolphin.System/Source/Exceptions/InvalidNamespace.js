Dolphin.namespace('Exceptions').InvalidNamespace = function(message) {
  this.message = message || '無效的命名空間';
  this.name = 'InvalidNamespace';
  this.stack = new Error().stack;
};
Dolphin.Exceptions.InvalidNamespace.prototype = Object.create(Error.prototype);
Dolphin.Exceptions.InvalidNamespace.prototype.constructor = Dolphin.Exceptions.InvalidNamespace;
