Dolphin.namespace('Exceptions').NullException = function(message) {
  this.message = message || '空值錯誤';
  this.name = 'NullException';
  this.stack = new Error().stack;
};
Dolphin.Exceptions.NullException.prototype = Object.create(Error.prototype);
Dolphin.Exceptions.NullException.prototype.constructor = Dolphin.Exceptions.NullException;
