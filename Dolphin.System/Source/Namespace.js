﻿function Namespace() {}

Namespace.prototype = {
  traverse: function(namespace, callback) {
    if (namespace.trim() === '') {
      return this;
    }

    var parts = namespace.split('.');
    for (var i = 0; i < parts.length; i++) {
      var part = parts[i].trim();
      if (part === '') {
        throw new Dolphin.Exceptions.InvalidNamespace();
      }
      parts[i] = part;
    }

    var node = this;
    parts.forEach(function(name) {
      node = callback(node, name);
    });

    return node;
  },

  namespace: function(namespace) {
    return this.traverse(namespace, function(node, item) {
      node[item] = node[item] || {};
      return node[item];
    });
  },

  reflection: function(namespace) {
    return this.traverse(namespace, function(node, item) {
      return node[item];
    });
  }
};

var Dolphin = new Namespace();
