Dolphin.namespace('').asString = function(object) {
  var dolphinTypes = Dolphin.Types;
  switch (Dolphin.getType(object)) {
    case dolphinTypes.Array:
      return 'array[' + object.length + ']';

    case dolphinTypes.Null:
      return 'null';

    case dolphinTypes.Undefined:
      return 'undefined';

    case dolphinTypes.Boolean:
      return 'boolean:' + object;

    case dolphinTypes.String:
      return "string:'" + object + "'";

    case dolphinTypes.Object:
      return 'object:' + JSON.stringify(object);

    case dolphinTypes.Number:
      return 'number:' + object;

    default:
      return 'Unknown';
  }
};

Dolphin.namespace('').deepEqual = function(object1, object2) {
  if (typeof object1 !== typeof object2) {
    return false;
  }

  if (typeof object1 === 'function') {
    return object1.toString() === object2.toString();
  }

  if (object1 instanceof Object && object2 instanceof Object) {
    if (Dolphin.propertyCount(object1) !== Dolphin.propertyCount(object2)) {
      return false;
    }
    var r = true;
    for (k in object1) {
      r = Dolphin.deepEqual(object1[k], object2[k]);
      if (!r) {
        return false;
      }
    }
    return true;
  } else {
    return object1 === object2;
  }
};

Dolphin.namespace('').getArgumentNames = function(func) {
  var ARROW = true;
  var FUNC_ARGS = ARROW
    ? /^(function)?\s*[^\(]*\(\s*([^\)]*)\)/m
    : /^(function)\s*[^\(]*\(\s*([^\)]*)\)/m;
  var FUNC_ARG_SPLIT = /,/;
  var FUNC_ARG = /^\s*(_?)(.+?)\1\s*$/;
  var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/gm;

  return ((func || '')
    .toString()
    .replace(STRIP_COMMENTS, '')
    .match(FUNC_ARGS) || ['', '', ''])[2]
    .split(FUNC_ARG_SPLIT)
    .map(function(arg) {
      return arg.replace(FUNC_ARG, function(all, underscore, name) {
        return name.split('=')[0].trim();
      });
    })
    .filter(String);
};

Dolphin.namespace('').getObjectSize = function(object) {
  return JSON.stringify(object).length;
};

Dolphin.namespace('').isEmpty = function(object) {
  var dolphinTypes = Dolphin.Types;
  switch (Dolphin.getType(object)) {
    case dolphinTypes.Array:
      return object.length < 1;

    case dolphinTypes.Null:
      return true;

    case dolphinTypes.Undefined:
      return true;

    case dolphinTypes.Boolean:
      return !object;

    case dolphinTypes.String:
      return object.length < 1;

    case dolphinTypes.Object:
      return Object.keys(object).length < 1;

    case dolphinTypes.Number:
      return object === 0;

    default:
      return false;
  }
};

Dolphin.namespace('').isNull = function(object) {
  return object === null || object === undefined;
};

Dolphin.namespace('').propertyCount = function(object) {
  var count = 0;
  for (var key in object) {
    if (object.hasOwnProperty(key)) {
      count++;
    }
  }
  return count;
};
