window.onerror = function(msg, url, lineNo, columnNo, error) {
  Dolphin.Tracer.log('>>>>>>>>>> ERROR <<<<<<<<<<');
  var string = msg.toLowerCase();
  var substring = 'script error';
  var handled = false;
  if (string.indexOf(substring) < 0) {
    var message = [
      'Message: ' + msg,
      'URL: ' + url,
      'Line: ' + lineNo,
      'Column: ' + columnNo,
      'Error object: ' + JSON.stringify(error)
    ].join(' - ');

    Dolphin.Tracer.log(message);
    handled = true;
  }
  Dolphin.Tracer.log('>>>>>>>>>> ERROR <<<<<<<<<<');
  return handled;
};
