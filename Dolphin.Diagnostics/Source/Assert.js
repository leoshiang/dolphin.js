Dolphin.namespace('').Assert = (function() {
  function throwException(exception, errorMessage) {
    if (exception === undefined) {
      throw new Dolphin.Exceptions.AssertionError(errorMessage);
    }
    throw new exception();
  }

  function equal(expected, actual, exception) {
    if (expected !== actual) {
      throwException(exception, '實際值與預期值不相同');
    }
  }

  function inRange(actual, min, max, exception) {
    if (actual < min || actual > max) {
      throwException(
        exception,
        Dolphin.String.format('實際值 {1}不在範圍內 {2} - {3}', actual, min, max)
      );
    }
  }

  function instanceOf(actual, expected, exception) {
    if (!(actual instanceof expected)) {
      throwException(exception, '實際值不是預期值的實例');
    }
  }

  function isArray(actual, exception) {
    if (!Array.isArray(actual)) {
      throwException(exception, '實際值不是陣列');
    }
  }

  function isString(actual, exception) {
    if (Object.prototype.toString.call(actual) !== '[object String]') {
      throwException(exception, '實際值不是字串');
    }
  }

  function isFunction(actual, exception) {
    if (typeof actual !== 'function') {
      throwException(exception, '實際值不是函數');
    }
  }

  function isNotEmpty(actual, exception) {
    if (Dolphin.isEmpty(actual)) {
      throwException(exception, '實際值為空');
    }
  }

  function isNull(actual, exception) {
    if (!Dolphin.isNull(actual)) {
      throwException(exception, '實際值不為 null');
    }
  }

  function isNotNull(actual, exception) {
    if (Dolphin.isNull(actual)) {
      throwException(exception, '實際值為 null');
    }
  }

  function notEqual(expected, actual, exception) {
    if (expected === actual) {
      throwException(exception, '實際值與預期值不應該相同');
    }
  }

  function typeOf(actual, expected, exception) {
    if (typeof actual !== expected) {
      throwException(exception, '實際值不是預期的型態: ' + expected);
    }
  }

  return {
    equal: equal,
    inRange: inRange,
    instanceOf: instanceOf,
    isArray: isArray,
    isString: isString,
    isFunction: isFunction,
    isNull: isNull,
    isNotEmpty: isNotEmpty,
    isNotNull: isNotNull,
    notEqual: notEqual,
    typeOf: typeOf
  };
})();
