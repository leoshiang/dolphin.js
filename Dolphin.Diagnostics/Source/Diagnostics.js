Dolphin.namespace('Diagnostics').tracerEnabled = true;

Dolphin.namespace('Diagnostics').traceObject = function(tagName, instance, className) {
  if (!Dolphin.Diagnostics.tracerEnabled) {
    return;
  }

  function makeObjectTraceable(instance) {
    if (Dolphin.isNull(instance)) {
      throw new Dolphin.Exceptions.InvalidArgument();
    }

    var result = Object(instance);
    for (var i = 1; i < arguments.length; i++) {
      var nextSource = arguments[i];
      if (nextSource === undefined || nextSource === null) {
        continue;
      }
      nextSource = Object(nextSource);

      var keysArray = Object.keys(nextSource);
      for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
        var nextKey = keysArray[nextIndex];
        var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
        if (desc !== undefined && desc.enumerable) {
          result[nextKey] = nextSource[nextKey];
        }
      }
    }
    return result;
  }

  function isMethodTraceable(methodName) {
    return (
      methodName !== '__exitMethod__' &&
      methodName !== '__enterMethod__' &&
      methodName !== '__before__' &&
      methodName !== '__after__' &&
      methodName !== 'prototype' &&
      methodName !== 'getArgumentNames' &&
      methodName !== '__getFullyQualifiedMethodName__'
    );
  }

  makeObjectTraceable(instance, Dolphin.Diagnostics.TracerMixin.prototype);
  var classNameNotAssigned = className === undefined || className === null || className === '';
  instance.__className__ = classNameNotAssigned ? tagName : className;
  instance.__tagName__ = tagName;
  for (var key in instance) {
    var method = instance[key];
    if (method instanceof Function || method === this) {
      if (isMethodTraceable(key)) {
        instance.__enterMethod__(key);
        instance.__exitMethod__(key);
      }
    }
  }
};

Dolphin.namespace('').new = function(tagName, constructor) {
  var args = [];
  for (var i = 1; i < arguments.length; i++) {
    args.push(arguments[i]);
  }

  var instance = new (Function.prototype.bind.apply(constructor, args))();

  if (Dolphin.Diagnostics.tracerEnabled) {
    Dolphin.Diagnostics.traceObject(tagName, instance, constructor.name);
  }

  return instance;
};
