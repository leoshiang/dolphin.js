Dolphin.namespace('').Tracer = (function() {
  var format = Dolphin.String.format;
  var repeat = Dolphin.String.repeat;
  var callStack = [];
  var options = {
    returnMark: '[return] ',
    enterMethodMark: '==> ',
    exitMethodMark: '<== ',
    indentSize: 4,
    indentChar: ' ',
    maxOutputLineLength: 80
  };

  function output(message) {
    var numOfPaddingChars = callStack.length * options.indentSize;
    var paddingChars = repeat(options.indentChar, numOfPaddingChars);
    var lineLength = options.maxOutputLineLength - numOfPaddingChars;
    var totalLength = message.length;
    var currentIndex = 0;
    while (currentIndex < totalLength) {
      var line = message.substring(currentIndex, currentIndex + lineLength);
      console.log(paddingChars + line);
      currentIndex += lineLength;
    }

    if (currentIndex < totalLength) {
      console.log(paddingChars + message.substring(currentIndex));
    }
  }

  function logObject(source) {
    output(Dolphin.asString(source));
  }

  function dumpParams(params, paramNames) {
    for (var i = 0; i < params.length; i++) {
      var arg = params[i];
      var text = Dolphin.asString(arg);
      var length = Dolphin.getObjectSize(text);
      var outputText = '[' + length + ']' + text;
      var paramName = paramNames[i] === undefined ? i : paramNames[i];
      output(format('({1}) {2}', paramName, outputText));
    }
  }

  function dumpReturnValue(value) {
    if (typeof value !== 'undefined') {
      var text = Dolphin.asString(value);
      var length = Dolphin.getObjectSize(text);
      var outputText = text;
      if (length > options.maxOutputLineLength) {
        outputText = '(' + length + ')';
      }
      output(options.returnMark + outputText);
    }
  }

  function enterMethod(methodName) {
    output(options.enterMethodMark + methodName);
    callStack.push(methodName);
  }

  function exitMethod() {
    var methodName = callStack.pop();
    if (methodName !== null) {
      output(options.exitMethodMark + methodName);
    } else {
      console.trace();
    }
  }

  return {
    log: output,
    logObject: logObject,
    enter: enterMethod,
    leave: exitMethod,
    printArguments: dumpParams,
    printReturnValue: dumpReturnValue
  };
})();
