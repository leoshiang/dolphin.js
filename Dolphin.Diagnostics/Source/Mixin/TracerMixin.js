Dolphin.namespace('Diagnostics').TracerMixin = function() {};

Dolphin.Diagnostics.TracerMixin.prototype = {
  __className__: '',
  __tagName__: '',

  __before__: function(property, fn) {
    var oldProperty = this[property];
    this[property] = function() {
      fn.apply(this, arguments);
      return oldProperty.apply(this, arguments);
    };
  },

  __after__: function(property, fn) {
    var oldProperty = this[property];
    this[property] = function() {
      oldProperty.apply(this, arguments);
      return fn.apply(this, arguments);
    };
  },

  __getFullyQualifiedMethodName__: function(instance, property) {
    var fullyQualifiedMethodName = instance.__className__ + '.' + property;
    if (!Dolphin.isEmpty(instance.__tagName__)) {
      fullyQualifiedMethodName += '[' + instance.__tagName__ + ']';
    }
    return fullyQualifiedMethodName;
  },

  __enterMethod__: function(property) {
    var oldProperty = this[property];
    this[property] = function() {
      var fullyQualifiedMethodName = this.__getFullyQualifiedMethodName__(this, property);
      Dolphin.Tracer.enter(fullyQualifiedMethodName);
      var argumentNames = Dolphin.getArgumentNames(oldProperty);
      Dolphin.Tracer.printArguments(arguments, argumentNames);
      return oldProperty.apply(this, arguments);
    };
  },

  __exitMethod__: function(property) {
    var oldProperty = this[property];
    this[property] = function() {
      var fullyQualifiedMethodName = this.__getFullyQualifiedMethodName__(this, property);
      var result = oldProperty.apply(this, arguments);
      Dolphin.Tracer.printReturnValue(result);
      Dolphin.Tracer.leave(fullyQualifiedMethodName);
      return result;
    };
  }
};
