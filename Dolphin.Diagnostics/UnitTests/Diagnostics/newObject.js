﻿QUnit.module('Dolphin.new');

QUnit.test('確認成員有被定義', function(assert) {
  assert.ok(typeof Dolphin.new === 'function');
});

QUnit.test('兩層套疊呼叫', function(assert) {
  function TestClass() {}

  TestClass.prototype.methodA = function() {
    return 'A';
  };
  TestClass.prototype.methodB = function() {
    this.methodA();
    return 'B';
  };
  var instance = Dolphin.new('testObject', TestClass);

  var oldLog = console.log;
  var traceLog = '';
  console.log = function(message) {
    traceLog += message;
    oldLog.apply(console, arguments);
  };

  instance.methodB();
  console.log = oldLog;
  assert.ok(
    traceLog ===
      "==> TestClass.methodB[testObject]    ==> TestClass.methodA[testObject]        [return] string:'A'    <== TestClass.methodA[testObject]    [return] string:'B'<== TestClass.methodB[testObject]"
  );
});
