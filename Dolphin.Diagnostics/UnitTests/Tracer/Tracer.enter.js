﻿QUnit.module('Dolphin.Tracer.enter');

QUnit.test('兩層套疊呼叫', function(assert) {
  var tracer = Dolphin.Tracer;

  // 攔截 console.log
  var oldLog = console.log;
  var traceLog = '';
  console.log = function(message) {
    traceLog += message;
    oldLog.apply(console, arguments);
  };

  tracer.enter('methodA');
  tracer.enter('methodB');
  tracer.leave();
  tracer.leave();

  console.log = oldLog;
  assert.ok(traceLog === '==> methodA    ==> methodB    <== methodB<== methodA');
});
