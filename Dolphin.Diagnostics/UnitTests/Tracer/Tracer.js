﻿QUnit.module('Dolphin.Tracer');

QUnit.test('確認成員有被定義', function(assert) {
  assert.equal(typeof Dolphin.Tracer, 'object');
  assert.equal(typeof Dolphin.Tracer.enter, 'function');
  assert.equal(typeof Dolphin.Tracer.leave, 'function');
  assert.equal(typeof Dolphin.Tracer.printArguments, 'function');
  assert.equal(typeof Dolphin.Tracer.printReturnValue, 'function');
  assert.equal(typeof Dolphin.Tracer.log, 'function');
  assert.equal(typeof Dolphin.Tracer.logObject, 'function');
});

QUnit.test('兩層套疊呼叫', function(assert) {
  var tracer = Dolphin.Tracer;

  // 攔截 console.log
  var oldLog = console.log;
  var traceLog = '';
  console.log = function(message) {
    traceLog += message;
    oldLog.apply(console, arguments);
  };

  tracer.enter('methodA');
  tracer.enter('methodB');
  tracer.leave();
  tracer.leave();

  console.log = oldLog;
  assert.ok(traceLog === '==> methodA    ==> methodB    <== methodB<== methodA');
});
