﻿QUnit.module('Dolphin.Diagnostics.TracerMixin');

QUnit.test('確認成員有被定義', function(assert) {
  assert.ok(typeof Dolphin.Diagnostics.TracerMixin === 'function');
  var mixin = new Dolphin.Diagnostics.TracerMixin();
  assert.ok(typeof mixin.__before__ === 'function');
  assert.ok(typeof mixin.__after__ === 'function');
  assert.ok(typeof mixin.__enterMethod__ === 'function');
  assert.ok(typeof mixin.__exitMethod__ === 'function');
  assert.ok(typeof mixin.__className__ === 'string');
});
