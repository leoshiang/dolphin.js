﻿QUnit.module('Dolphin.Assert');

QUnit.test('確認成員有被定義', function(assert) {
  assert.equal(typeof Dolphin.Assert.equal, 'function');
  assert.equal(typeof Dolphin.Assert.inRange, 'function');
  assert.equal(typeof Dolphin.Assert.instanceOf, 'function');
  assert.equal(typeof Dolphin.Assert.isArray, 'function');
  assert.equal(typeof Dolphin.Assert.isString, 'function');
  assert.equal(typeof Dolphin.Assert.isFunction, 'function');
  assert.equal(typeof Dolphin.Assert.isNull, 'function');
  assert.equal(typeof Dolphin.Assert.isNotEmpty, 'function');
  assert.equal(typeof Dolphin.Assert.isNotNull, 'function');
  assert.equal(typeof Dolphin.Assert.notEqual, 'function');
  assert.equal(typeof Dolphin.Assert.typeOf, 'function');
});
