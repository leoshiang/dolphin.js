Dolphin.namespace('').Assert = (function() {
  function throwException(exception, errorMessage) {
    if (exception === undefined) {
      throw new Dolphin.Exceptions.AssertionError(errorMessage);
    }
    throw new exception();
  }

  function equal(expected, actual, exception) {
    if (expected !== actual) {
      throwException(exception, '實際值與預期值不相同');
    }
  }

  function inRange(actual, min, max, exception) {
    if (actual < min || actual > max) {
      throwException(
        exception,
        Dolphin.String.format('實際值 {1}不在範圍內 {2} - {3}', actual, min, max)
      );
    }
  }

  function instanceOf(actual, expected, exception) {
    if (!(actual instanceof expected)) {
      throwException(exception, '實際值不是預期值的實例');
    }
  }

  function isArray(actual, exception) {
    if (!Array.isArray(actual)) {
      throwException(exception, '實際值不是陣列');
    }
  }

  function isString(actual, exception) {
    if (Object.prototype.toString.call(actual) !== '[object String]') {
      throwException(exception, '實際值不是字串');
    }
  }

  function isFunction(actual, exception) {
    if (typeof actual !== 'function') {
      throwException(exception, '實際值不是函數');
    }
  }

  function isNotEmpty(actual, exception) {
    if (Dolphin.isEmpty(actual)) {
      throwException(exception, '實際值為空');
    }
  }

  function isNull(actual, exception) {
    if (!Dolphin.isNull(actual)) {
      throwException(exception, '實際值不為 null');
    }
  }

  function isNotNull(actual, exception) {
    if (Dolphin.isNull(actual)) {
      throwException(exception, '實際值為 null');
    }
  }

  function notEqual(expected, actual, exception) {
    if (expected === actual) {
      throwException(exception, '實際值與預期值不應該相同');
    }
  }

  function typeOf(actual, expected, exception) {
    if (typeof actual !== expected) {
      throwException(exception, '實際值不是預期的型態: ' + expected);
    }
  }

  return {
    equal: equal,
    inRange: inRange,
    instanceOf: instanceOf,
    isArray: isArray,
    isString: isString,
    isFunction: isFunction,
    isNull: isNull,
    isNotEmpty: isNotEmpty,
    isNotNull: isNotNull,
    notEqual: notEqual,
    typeOf: typeOf
  };
})();

Dolphin.namespace('Diagnostics').TracerMixin = function() {};

Dolphin.Diagnostics.TracerMixin.prototype = {
  __className__: '',
  __tagName__: '',

  __before__: function(property, fn) {
    var oldProperty = this[property];
    this[property] = function() {
      fn.apply(this, arguments);
      return oldProperty.apply(this, arguments);
    };
  },

  __after__: function(property, fn) {
    var oldProperty = this[property];
    this[property] = function() {
      oldProperty.apply(this, arguments);
      return fn.apply(this, arguments);
    };
  },

  __getFullyQualifiedMethodName__: function(instance, property) {
    var fullyQualifiedMethodName = instance.__className__ + '.' + property;
    if (!Dolphin.isEmpty(instance.__tagName__)) {
      fullyQualifiedMethodName += '[' + instance.__tagName__ + ']';
    }
    return fullyQualifiedMethodName;
  },

  __enterMethod__: function(property) {
    var oldProperty = this[property];
    this[property] = function() {
      var fullyQualifiedMethodName = this.__getFullyQualifiedMethodName__(this, property);
      Dolphin.Tracer.enter(fullyQualifiedMethodName);
      var argumentNames = Dolphin.getArgumentNames(oldProperty);
      Dolphin.Tracer.printArguments(arguments, argumentNames);
      return oldProperty.apply(this, arguments);
    };
  },

  __exitMethod__: function(property) {
    var oldProperty = this[property];
    this[property] = function() {
      var fullyQualifiedMethodName = this.__getFullyQualifiedMethodName__(this, property);
      var result = oldProperty.apply(this, arguments);
      Dolphin.Tracer.printReturnValue(result);
      Dolphin.Tracer.leave(fullyQualifiedMethodName);
      return result;
    };
  }
};

Dolphin.namespace('').Tracer = (function() {
  var format = Dolphin.String.format;
  var repeat = Dolphin.String.repeat;
  var callStack = [];
  var options = {
    returnMark: '[return] ',
    enterMethodMark: '==> ',
    exitMethodMark: '<== ',
    indentSize: 4,
    indentChar: ' ',
    maxOutputLineLength: 80
  };

  function output(message) {
    var numOfPaddingChars = callStack.length * options.indentSize;
    var paddingChars = repeat(options.indentChar, numOfPaddingChars);
    var lineLength = options.maxOutputLineLength - numOfPaddingChars;
    var totalLength = message.length;
    var currentIndex = 0;
    while (currentIndex < totalLength) {
      var line = message.substring(currentIndex, currentIndex + lineLength);
      console.log(paddingChars + line);
      currentIndex += lineLength;
    }

    if (currentIndex < totalLength) {
      console.log(paddingChars + message.substring(currentIndex));
    }
  }

  function logObject(source) {
    output(Dolphin.asString(source));
  }

  function dumpParams(params, paramNames) {
    for (var i = 0; i < params.length; i++) {
      var arg = params[i];
      var text = Dolphin.asString(arg);
      var length = Dolphin.getObjectSize(text);
      var outputText = '[' + length + ']' + text;
      var paramName = paramNames[i] === undefined ? i : paramNames[i];
      output(format('({1}) {2}', paramName, outputText));
    }
  }

  function dumpReturnValue(value) {
    if (typeof value !== 'undefined') {
      var text = Dolphin.asString(value);
      var length = Dolphin.getObjectSize(text);
      var outputText = text;
      if (length > options.maxOutputLineLength) {
        outputText = '(' + length + ')';
      }
      output(options.returnMark + outputText);
    }
  }

  function enterMethod(methodName) {
    output(options.enterMethodMark + methodName);
    callStack.push(methodName);
  }

  function exitMethod() {
    var methodName = callStack.pop();
    if (methodName !== null) {
      output(options.exitMethodMark + methodName);
    } else {
      console.trace();
    }
  }

  return {
    log: output,
    logObject: logObject,
    enter: enterMethod,
    leave: exitMethod,
    printArguments: dumpParams,
    printReturnValue: dumpReturnValue
  };
})();

Dolphin.namespace('Diagnostics').tracerEnabled = true;

Dolphin.namespace('Diagnostics').traceObject = function(tagName, instance, className) {
  if (!Dolphin.Diagnostics.tracerEnabled) {
    return;
  }

  function makeObjectTraceable(instance) {
    if (Dolphin.isNull(instance)) {
      throw new Dolphin.Exceptions.InvalidArgument();
    }

    var result = Object(instance);
    for (var i = 1; i < arguments.length; i++) {
      var nextSource = arguments[i];
      if (nextSource === undefined || nextSource === null) {
        continue;
      }
      nextSource = Object(nextSource);

      var keysArray = Object.keys(nextSource);
      for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
        var nextKey = keysArray[nextIndex];
        var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
        if (desc !== undefined && desc.enumerable) {
          result[nextKey] = nextSource[nextKey];
        }
      }
    }
    return result;
  }

  function isMethodTraceable(methodName) {
    return (
      methodName !== '__exitMethod__' &&
      methodName !== '__enterMethod__' &&
      methodName !== '__before__' &&
      methodName !== '__after__' &&
      methodName !== 'prototype' &&
      methodName !== 'getArgumentNames' &&
      methodName !== '__getFullyQualifiedMethodName__'
    );
  }

  makeObjectTraceable(instance, Dolphin.Diagnostics.TracerMixin.prototype);
  var classNameNotAssigned = className === undefined || className === null || className === '';
  instance.__className__ = classNameNotAssigned ? tagName : className;
  instance.__tagName__ = tagName;
  for (var key in instance) {
    var method = instance[key];
    if (method instanceof Function || method === this) {
      if (isMethodTraceable(key)) {
        instance.__enterMethod__(key);
        instance.__exitMethod__(key);
      }
    }
  }
};

Dolphin.namespace('').new = function(tagName, constructor) {
  var args = [];
  for (var i = 1; i < arguments.length; i++) {
    args.push(arguments[i]);
  }

  var instance = new (Function.prototype.bind.apply(constructor, args))();

  if (Dolphin.Diagnostics.tracerEnabled) {
    Dolphin.Diagnostics.traceObject(tagName, instance, constructor.name);
  }

  return instance;
};

window.onerror = function(msg, url, lineNo, columnNo, error) {
  Dolphin.Tracer.log('>>>>>>>>>> ERROR <<<<<<<<<<');
  var string = msg.toLowerCase();
  var substring = 'script error';
  var handled = false;
  if (string.indexOf(substring) < 0) {
    var message = [
      'Message: ' + msg,
      'URL: ' + url,
      'Line: ' + lineNo,
      'Column: ' + columnNo,
      'Error object: ' + JSON.stringify(error)
    ].join(' - ');

    Dolphin.Tracer.log(message);
    handled = true;
  }
  Dolphin.Tracer.log('>>>>>>>>>> ERROR <<<<<<<<<<');
  return handled;
};