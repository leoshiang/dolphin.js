﻿QUnit.module('Dolphin.Project.Resource');

QUnit.test('測試 Resource 有被定義', function(assert) {
  assert.equal(typeof Dolphin.Project.Resource, 'function');
});

QUnit.test('測試 Resource 成員有被定義', function(assert) {
  var resource = new Dolphin.Project.Resource();
  assert.equal(typeof resource.name, 'string');
});
