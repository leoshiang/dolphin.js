﻿QUnit.module('Dolphin.Project.Task');

QUnit.test('測試 Task 有被定義', function(assert) {
  assert.equal(typeof Dolphin.Project.Task, 'function');
});

QUnit.test('確認成員有被定義', function(assert) {
  var task = new Dolphin.Project.Task();
  assert.ok(task.dateRange instanceof Dolphin.DateTime.DateRange);
  assert.equal(typeof task.name, 'string');
  assert.equal(typeof task.progress, 'number');
  assert.ok(task.resources instanceof Dolphin.Project.ResourceList);
  assert.equal(typeof task.status, 'string');
  assert.equal(typeof task.type, 'string');
  assert.equal(typeof task.notes, 'string');
});
