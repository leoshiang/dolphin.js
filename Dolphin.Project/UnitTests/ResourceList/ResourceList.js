﻿QUnit.module('Dolphin.Project.ResourceList');

QUnit.test('測試 ResourceList 有被定義', function(assert) {
  assert.equal(typeof Dolphin.Project.ResourceList, 'function');
});

QUnit.test('測試 ResourceList 成員有被定義', function(assert) {
  var resourceList = new Dolphin.Project.ResourceList();
  assert.equal(typeof resourceList.items, 'object');
});
