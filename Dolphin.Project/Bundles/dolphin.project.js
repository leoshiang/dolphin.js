Dolphin.namespace('Project').Resource = function() {
  this.name = '';
};

Dolphin.namespace('Project').ResourceList = function() {
  this.items = [];
};

Dolphin.Collections.ListMixin.call(Dolphin.Project.ResourceList.prototype);

Dolphin.namespace('Project').TaskStatus = {
  Active: 'active',
  Finished: 'finished',
  Pending: 'pending',
  Skipped: 'skipped'
};

Dolphin.namespace('Project').Task = function() {
  this.dateRange = new Dolphin.DateTime.DateRange();
  this.name = '';
  this.progress = 0.0;
  this.resources = new Dolphin.Project.ResourceList();
  this.status = Dolphin.Project.TaskStatus.Pending;
  this.type = '';
  this.notes = '';
};

Dolphin.namespace('Project').TaskList = function() {
  this.items = [];
};

Dolphin.Collections.ListMixin.call(Dolphin.Project.TaskList.prototype);
