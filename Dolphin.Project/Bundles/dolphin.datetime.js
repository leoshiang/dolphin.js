Dolphin.namespace('').DateTime = (function() {
  function daysToSec(days) {
    return 60 * 60 * 24 * days;
  }

  function daysToMSec(days) {
    return 1000 * daysToSec(days);
  }

  return {
    daysToSec: daysToSec,
    daysToMSec: daysToMSec
  };
})();
Dolphin.namespace('DateTime').DateRange = function() {
  this.startDate = null;
  this.endDate = null;
  if (arguments.length === 2) {
    this.setRange(arguments[0], arguments[1]);
  }
};

Dolphin.DateTime.DateRange.prototype = {
  clear: function() {
    this.startDate = null;
    this.endDate = null;
  },

  clone: function() {
    return new Dolphin.DateTime.DateRange(this.startDate, this.endDate);
  },

  contains: function(date) {
    return date >= this.startDate && date <= this.endDate;
  },

  equalTo: function(range) {
    return this.startDate === range.startDate && this.endDate === range.endDate;
  },

  getDuration: function() {
    var oneDay = Dolphin.DateTime.daysToMSec(1);
    var endDay = Math.floor(this.endDate.getTime() / oneDay);
    var startDay = Math.floor(this.startDate.getTime() / oneDay);
    return endDay - startDay + 1;
  },

  intersects: function(range) {
    var containsStartDate = this.contains(range.startDate) && !this.contains(range.endDate);
    var containsEndDate = !this.contains(range.startDate) && this.contains(range.endDate);
    var inside = this.contains(range.startDate) && this.contains(range.endDate);
    var contains = range.startDate <= this.startDate && range.endDate >= this.endDate;
    return containsStartDate || containsEndDate || inside || contains;
  },

  union: function(range) {
    var startDate = new Date(Math.min(this.startDate.getTime(), range.startDate.getTime()));
    var endDate = new Date(Math.max(this.endDate.getTime(), range.endDate.getTime()));
    return new Dolphin.DateTime.DateRange(startDate, endDate);
  },

  setRange: function(startDate, endDate) {
    this.startDate = startDate;
    this.endDate = endDate;
  }
};
Dolphin.namespace('DateTime').DateRangeList = function() {
  this.items = new Dolphin.Collections.List();
  this.bounds = new Dolphin.DateTime.DateRange();
};

Dolphin.DateTime.DateRangeList.prototype = {
  addArray: function(array) {
    Dolphin.Assert(array, Array);
    this.beginUpdate();
    try {
      array.forEach(
        function(item) {
          this.addRange(item);
        }.bind(this)
      );
    } catch (err) {
      console.log(err);
    } finally {
      this.endUpdate();
    }
    return this;
  },

  addList: function(list) {
    Dolphin.Assert(list, Dolphin.Collections.List);
    this.beginUpdate();
    try {
      list.forEach(
        function(item) {
          this.addRange(item);
        }.bind(this)
      );
    } catch (err) {
      console.log(err);
    } finally {
      this.endUpdate();
    }
    return this;
  },

  addRange: function(range) {
    Dolphin.Assert(range, Dolphin.DateTime.DateRange);
    this.items.add(range);
    this.onUpdated();
    return this;
  },

  intersection: function(range) {
    Dolphin.Assert(range, Dolphin.DateTime.DateRange);
    var result = new Dolphin.Collections.List();
    this.items.forEach(function(item) {
      if (item.intersects(range)) {
        result.add(item);
      }
    });
    return result;
  },

  sort: function() {
    this.items.sort(function(a, b) {
      if (a.startDate < b.startDate) {
        return -1;
      } else if (a.startDate > b.startDate) {
        return 1;
      } else {
        return 0;
      }
    });
    return this;
  },

  onUpdated: function() {
    if (this.updating) {
      return;
    }

    if (this.items.isEmpty()) {
      this.bounds.clear();
    } else {
      this.bounds = this.items.get(0);
      this.items.forEach(
        function(item) {
          this.bounds = this.bounds.union(item);
        }.bind(this)
      );
    }
  }
};

Dolphin.Collections.ListMixin.call(Dolphin.DateTime.DateRangeList.prototype);
Dolphin.Collections.LockingMixin.call(Dolphin.DateTime.DateRangeList.prototype);
