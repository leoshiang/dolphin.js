Dolphin.namespace('Project').TaskStatus = {
  Active: 'active',
  Finished: 'finished',
  Pending: 'pending',
  Skipped: 'skipped'
};
