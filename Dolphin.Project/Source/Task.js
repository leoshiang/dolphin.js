Dolphin.namespace('Project').Task = function() {
  this.dateRange = new Dolphin.DateTime.DateRange();
  this.name = '';
  this.progress = 0.0;
  this.resources = new Dolphin.Project.ResourceList();
  this.status = Dolphin.Project.TaskStatus.Pending;
  this.type = '';
  this.notes = '';
};
