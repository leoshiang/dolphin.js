Dolphin.namespace('').String = (function() {
  function format(source) {
    if (arguments.length <= 1) {
      throw new Dolphin.Exceptions.InvalidArgument();
    }

    var result = source;
    for (var i = 1; i < arguments.length; i++) {
      var regex = new RegExp('\\{' + i + '\\}', 'gi');
      if (!regex.test(result)) {
        throw new Dolphin.Exceptions.InvalidArgument();
      }
      result = result.replace(regex, arguments[i]);
    }

    return result;
  }

  function rightPad(source, paddingString, length) {
    var result = source;

    if (result.length >= length) {
      return result;
    }

    while (result.length < length) {
      result = paddingString + result;
    }

    return result;
  }

  function repeat(source, count) {
    if (Dolphin.isNull(source)) {
      throw new Dolphin.Exceptions.InvalidArgument();
    }

    if (count < 0) {
      throw new Dolphin.Exceptions.InvalidArgument();
    }

    var result = [];
    result.length = count + 1;

    return result.join(source);
  }

  return {
    format: format,
    rightPad: rightPad,
    repeat: repeat
  };
})();