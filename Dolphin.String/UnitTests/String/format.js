﻿QUnit.module('Dolphin.String.format');

QUnit.test('沒有參數，應拋出 InvalidArgument 例外', function(assert) {
  assert.throws(
    function() {
      var result = Dolphin.String.format();
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.InvalidArgument;
    }
  );
});

QUnit.test('有參數沒有值，應拋出 InvalidArgument 例外', function(assert) {
  assert.throws(
    function() {
      var result = Dolphin.String.format('{1}');
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.InvalidArgument;
    }
  );
});

QUnit.test('參數比值多，應有參數未被取代', function(assert) {
  assert.equal(Dolphin.String.format('{1}{2}{3}', 1, 2), '12{3}');
});

QUnit.test('參數比值少，應拋出 InvalidArgument 例外', function(assert) {
  assert.throws(
    function() {
      var result = Dolphin.String.format('{1}', 1, 2);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.InvalidArgument;
    }
  );
});

QUnit.test('沒有參數有值，應拋出 InvalidArgument 例外', function(assert) {
  assert.throws(
    function() {
      var result = Dolphin.String.format('', 'test');
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.InvalidArgument;
    }
  );
});

QUnit.test('參數編號亂跳，應能正確取代變數', function(assert) {
  assert.equal(Dolphin.String.format('{3}{2}{1}', 1, 2, 3), '321');
});

QUnit.test('參數編號大於值的數目，應拋出 InvalidArgument 例外', function(assert) {
  assert.throws(
    function() {
      var result = Dolphin.String.format('{4}{5}{6}', 1, 2, 3);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.InvalidArgument;
    }
  );
});

QUnit.test('值為 null，應可取代變數', function(assert) {
  assert.equal(Dolphin.String.format('{1}{2}{3}', null, null, null), 'nullnullnull');
});

QUnit.test('數字參數，應可取代變數', function(assert) {
  assert.equal(Dolphin.String.format('{1}{2}{3}', 1, 2, 3), '123');
});

QUnit.test('文字參數，應可取代變數', function(assert) {
  assert.equal(Dolphin.String.format('{1}{2}{3}', 'A', 'B', 'C'), 'ABC');
});
