﻿QUnit.module('Dolphin.String');

QUnit.test('確認成員有被定義', function(assert) {
  assert.equal(typeof Dolphin.String.format, 'function');
  assert.equal(typeof Dolphin.String.repeat, 'function');
  assert.equal(typeof Dolphin.String.rightPad, 'function');
});
