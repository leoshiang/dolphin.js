﻿QUnit.module('Dolphin.String.repeat');

QUnit.test('沒有參數，應拋出 InvalidArgument 例外', function(assert) {
  assert.throws(
    function() {
      var result = Dolphin.String.repeat();
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.InvalidArgument;
    }
  );
});

QUnit.test('count < 0，應拋出 InvalidArgument 例外', function(assert) {
  assert.throws(
    function() {
      var result = Dolphin.String.repeat('a', -1);
    },
    function(error) {
      return error instanceof Dolphin.Exceptions.InvalidArgument;
    }
  );
});

QUnit.test('單一字元重複0次，應回傳空字串', function(assert) {
  var result = Dolphin.String.repeat('a', 0);
  assert.equal(result, '');
});

QUnit.test('單一字元重複3次，應回傳3個字元', function(assert) {
  var result = Dolphin.String.repeat('a', 3);
  assert.equal(result, 'aaa');
});

QUnit.test('三個字元重複0次，應回傳空字串', function(assert) {
  var result = Dolphin.String.repeat('abc', 0);
  assert.equal(result, '');
});

QUnit.test('三個字元重複3次，應回傳9個字元', function(assert) {
  var result = Dolphin.String.repeat('abc', 3);
  assert.equal(result, 'abcabcabc');
});
