﻿QUnit.module('Dolphin.String.rightPad');

QUnit.test('沒有參數', function(assert) {
  var rightPad = Dolphin.String.rightPad;
  var output = rightPad('a', '*', 3);
  assert.ok(output === '**a');
});
